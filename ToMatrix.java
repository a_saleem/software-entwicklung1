public class ToMatrix {

    public static void main(String[] args) {
        while (!StdIn.isEmpty()) {
            int a = StdIn.readInt();
            // Berechnung der Hunderter-, Zehner- und Einerstelle
            if (a > 999) a=999;
            int ah = a/100;
            int az = (a-(ah*100))/10;
            int ae = a-(ah*100)-(az*10);
            //Führende Null? --> Defaultbedingung gibt schwarz aus
            if (ah==0) {
               ah=10;
               if (az==0) az=10;
               }
            //Darstellung und Ausgabe
            Ausgabe(Array(ah), Array(az), Array(ae));
        }
    }

    public static int[][] Array(int a) {
        //Darstellung der Zahlen
        int[][] a0 =
        {
            {1,1,1},
            {1,0,1},
            {1,0,1},
            {1,0,1},
            {1,1,1}
        };
        int[][] a1 =
        {
            {0,0,1},
            {0,0,1},
            {0,0,1},
            {0,0,1},
            {0,0,1}
        }; 
        int[][] a2 =
        {
            {1,1,1},
            {0,0,1},
            {1,1,1},
            {1,0,0},
            {1,1,1}
        }; 
        int[][] a3 =
        {
            {1,1,1},
            {0,0,1},
            {1,1,1},
            {0,0,1},
            {1,1,1}
        }; 
        int[][] a4 =
        {
            {1,0,0},
            {1,0,0},
            {1,1,1},
            {0,1,0},
            {0,1,0}
        }; 
        int[][] a5 =
        {
            {1,1,1},
            {1,0,0},
            {1,1,1},
            {0,0,1},
            {1,1,1}
        }; 
        int[][] a6 =
        {
            {1,0,0},
            {1,0,0},
            {1,1,1},
            {1,0,1},
            {1,1,1}
        }; 
        int[][] a7 =
        {
            {1,1,1},
            {0,0,1},
            {0,0,1},
            {0,0,1},
            {0,0,1}
        }; 
        int[][] a8 =
        {
            {1,1,1},
            {1,0,1},
            {1,1,1},
            {1,0,1},
            {1,1,1}
        }; 
        int[][] a9 =
        {
            {1,1,1},
            {1,0,1},
            {1,1,1},
            {0,0,1},
            {0,0,1}
        }; 
        int[][] aoff =
        {
            {0,0,0},
            {0,0,0},
            {0,0,0},
            {0,0,0},
            {0,0,0}
        }; 

                switch (a) {
                    case 0:
                        return a0;
                    case 1:
                        return a1;
                    case 2:
                        return a2;
                    case 3:
                        return a3;  
                    case 4:
                        return a4;
                    case 5:
                        return a5;
                    case 6:
                        return a6;
                    case 7:
                        return a7;     
                    case 8:
                        return a8;
                    case 9:
                        return a9;
                    default:
                        return aoff;                   
                }
    }

    public static void Ausgabe(int[][] arh, int[][] arz, int[][] are) {
        for (int i=0; i<arh.length; i++) {
            for (int j=0; j<arh[i].length; j++)
                System.out.print(arh[i][j]);
            for (int j=0; j<arz[i].length; j++)
                System.out.print(arz[i][j]);
            for (int j=0; j<are[i].length; j++)    
                System.out.print(are[i][j]);
            System.out.println();
        }
    
    }
}