import java.util.Arrays;

public class StringReader{
	
	public static void main(String[] args){
		
		readStrings();
		
	}
	
	public static void readStrings(){
		
		String[] values = new String[5];
		
		int index = 0;
		while(!StdIn.isEmpty()){
			
			
			values[index] = StdIn.readString();
			
			//Falls Array zu klein Werte in gr��eren kopieren
			if (index == values.length - 1){
				
				String[] tmp = new String[values.length * 2];
				
				//Werte �bertragen
				for (int i = 0; i < values.length; i++){
					
					tmp[i] = values[i];
					
				}
				
				values = tmp;
				
			}
			
			index++;
			
		}
		
		//Elemente z�hlen die nicht null sind
		int count = 0;
		for(int i = 0; i < values.length; i++){
			if(values[i] != null){
				count++;
			}
						
		}
		String[] tmp = new String[count];
		
		for(int i = 0; i < count; i++){
			tmp [i] = values[i];
		}
		
		StdOut.println(Arrays.toString(tmp));
		
	}
	
}