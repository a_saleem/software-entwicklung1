import java.util.Arrays;

public class ArraysAndProcedures {

    public static void main(String[] args) {
        // Testen von Aufgabenteilen

    }

    public static void replaceAll(int x, int y, int[] ar) {
        // TODO Aufgabe a)
        for (int i=0; i<ar.length;i++)
            if (ar[i]==x) ar[i]=y;
    }

    public static void replaceFirst(int x, int y, int[] ar) {
        // TODO Aufgabe b)
        for (int i=0; i<ar.length;i++)
            if (ar[i]==x) {
                ar[i]=y;
                break;
            }
    }

    public static void replaceLast(int x, int y, int[] ar) {
        // TODO Aufgabe b)
        for (int i=ar.length-1; i>=0;i--)
            if (ar[i]==x) {
                ar[i]=y;
                break;
        }
    }

    public static int[] substAll(int x, int y, int[] ar) {
        // TODO Aufgabe c)
        int[] ar2 = new int[ar.length];
        for (int i=0; i<ar.length; i++)
            if (ar[i]==x) ar2[i]=y;
            else ar2[i]=ar[i];
        return ar2;
    }

    public static int[] onlyEven(int[] ar) {
        // TODO Aufgabe d)
        int n=0;
        for (int i=0; i<ar.length; i++)
            if (ar[i]%2==0) n++;
        int[] ar3 = new int[n];
        for (int i=0, m=0; i<ar.length; i++)
            if (ar[i]%2==0) {
                ar3[m]=ar[i];
                m++;
            }
        return ar3;
    }

    public static boolean allHaveZero(int[][] arrays) {
        // TODO Aufgabe e)
        boolean allHaveZero=true;
        for (int i=0; i<arrays.length; i++)
            for (int j=0; j<arrays[i].length; j++)
                if (arrays[i][j]==0) break;
                else if (j==arrays[i].length-1)allHaveZero=false;
        return allHaveZero;
    }

}