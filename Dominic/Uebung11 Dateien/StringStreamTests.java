import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class StringStreamTests {


    @Rule
    public Timeout globalTimeout = Timeout.seconds(5);

    @Test
    public void testEmpty() throws IOException {
        Reader r = new StringReader("");
        WordStream ws = new WordStream(r);
        assertEquals(null, ws.read());
    }

    @Test
    public void testOneWord() throws IOException {
        Reader r = new StringReader("hello");
        WordStream ws = new WordStream(r);
        assertEquals("hello", ws.read());
        assertEquals(null, ws.read());
    }

    @Test
    public void testOneWordPadLeft() throws IOException {
        Reader r = new StringReader("   hello");
        WordStream ws = new WordStream(r);
        assertEquals("hello", ws.read());
        assertEquals(null, ws.read());
    }

    @Test
    public void testOneWordPadRight() throws IOException {
        Reader r = new StringReader("hello   ");
        WordStream ws = new WordStream(r);
        assertEquals("hello", ws.read());
        assertEquals(null, ws.read());
    }

    @Test
    public void testSentence() throws IOException {
        Reader r = new StringReader("How are you, Donald?");
        WordStream ws = new WordStream(r);
        assertEquals("How", ws.read());
        assertEquals("are", ws.read());
        assertEquals("you", ws.read());
        assertEquals("Donald", ws.read());
        assertEquals(null, ws.read());
    }

    @Test
    public void testInfiniteStream() throws IOException {

        class MyReader extends Reader {
            String input = "bla blub ";
            StringReader reader = new StringReader(input);
            int pos = 0;

            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                int read = reader.read(cbuf, off, len);
                if (read > 0) {
                    return read;
                }
                reader.reset();
                return reader.read(cbuf, off, len);
            }

            @Override
            public void close() throws IOException {
                reader.close();
            }
        }


        Reader r = new MyReader();
        WordStream ws = new WordStream(r);
        assertEquals("bla", ws.read());
        assertEquals("blub", ws.read());
        assertEquals("bla", ws.read());
        assertEquals("blub", ws.read());
        assertEquals("bla", ws.read());
        assertEquals("blub", ws.read());
    }
}
