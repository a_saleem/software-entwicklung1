import org.junit.Test;
import static org.junit.Assert.*;

public class QueueTest {

    @Test
    public void simple() throws Exception {
        Queue q = new ArrayQueue(5);
        q.add("a");
        q.add("b");
        assertEquals("a", q.remove());
        assertEquals("b", q.remove());
        try {
            q.remove();
            fail("Expected exception.");
        } catch (QueueEmptyException e) {
            return;
        }
    }

    @Test
    public void interleaved() throws Exception {
        Queue q = new ArrayQueue(5);
        q.add("a");
        q.add("b");
        assertEquals("a", q.remove());
        q.add("c");
        assertEquals("b", q.remove());
        assertEquals("c", q.remove());
        try {
            q.remove();
            fail("Expected exception.");
        } catch (QueueEmptyException e) {
            return;
        }
    }

    @Test
    public void testelement() throws Exception {
        Queue q = new ArrayQueue(5);
        q.add("a");
        q.add("b");
        assertEquals("a", q.element());
        q.remove();
        assertEquals("b", q.element());
    }

    @Test
    public void testIsEmpty() throws Exception {
        Queue q = new ArrayQueue(5);
        assertTrue(q.isEmpty());
        q.add("a");
        assertFalse(q.isEmpty());
        q.add("b");
        assertFalse(q.isEmpty());
        q.remove();
        assertFalse(q.isEmpty());
        q.remove();
        assertTrue(q.isEmpty());
    }

    @Test
    public void testSize1() throws Exception {
        Queue q = new ArrayQueue(1);
        q.add("a");
        try {
            q.add("b");
            fail("Queue full");
        } catch (QueueFullException e) {
            return;
        }
        fail("Expected exception");
    }

    @Test
    public void testWraparound() throws Exception {
        String[] nodes = {"a", "b", "c"};

        Queue q = new ArrayQueue(5);
        q.add("a");
        q.add("b");
        q.add("c");
        for (int i = 0; i <= 50; i++) {
            q.add(nodes[i % 3]);
            assertEquals(nodes[i % 3], q.remove());
        }
        assertEquals("a", q.remove());
        assertEquals("b", q.remove());
        assertEquals("c", q.remove());
        try {
            q.remove();
            fail("expected exception");
        } catch (QueueEmptyException e) {
            return;
        }
    }

}
