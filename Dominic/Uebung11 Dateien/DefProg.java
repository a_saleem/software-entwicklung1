import java.util.List;

class DefProg {

  /*
   * requires entries != null
   *       && key != null
   *       && für alle e in entries: e != null
   *       && entries enthält einen Eintrag e mit key.equals(e.getKey())
   * returns e.getValue(), wobei e der erste Eintrag in entries
   *                       mit key.equals(e.getKey()) ist
   */
  public static Object find(List<Entry> entries, String key) {
    for (Entry e : entries) {
      if (key.equals(e.getKey())) {
        return e.getValue();
      }
    }
    return null;
  }

}