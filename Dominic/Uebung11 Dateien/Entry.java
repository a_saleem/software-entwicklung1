interface Entry {
  String getKey();
  Object getValue();
}

