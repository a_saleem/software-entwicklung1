import java.io.IOException;

interface StringStream {
  // Liefert das naechte Element im Stream oder null wenn das Ende erreicht ist
  String read() throws IOException;
}