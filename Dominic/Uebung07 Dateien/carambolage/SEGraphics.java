import java.awt.*;

public class SEGraphics {

	private Graphics2D g;
	private int breite;
	private int hoehe;
	
	public SEGraphics(Graphics2D g, int breite, int hoehe) {
		this.g = g;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	/** fuellt das Bild mit der gegebenen Farbe */
	public void reset(Color color) {
		g.setColor(color);
		g.fillRect(0, 0, breite, hoehe);
	}

	/** zeichnet einen Kreis mit Angabe von Mittelpunkt (x,y), Radius und Fuellfarbe */
	public void drawCircle(double x, double y, double radius, Color c) {
		g.setColor(c);
		g.fillOval((int) (x-radius), (int) (y-radius), (int) (2*radius), (int) (2*radius));
	}

	/** zeichnet den gegeben Text */
	public void drawString(String str, double x, double y, Color c) {
		g.setColor(c);
		g.drawString(str, (int) x, (int) y);
	}

	/** zeichnet eine Linie von (x1,y1) zu (x2,y2) */
	public void drawLine(double x1, double y1, double x2, double y2, Color c, float width) {
		g.setColor(c);
		g.setStroke(new BasicStroke(width));
		g.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
	}


}
