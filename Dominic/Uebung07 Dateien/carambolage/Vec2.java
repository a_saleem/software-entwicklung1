public class Vec2 {
    final double x;
    final double y;

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double length() {
        double l = Math.sqrt(x * x + y * y);
        return l;
    }

    public Vec2 mult(double factor) {
        Vec2 vec = new Vec2(x * factor, y * factor);
        return vec;
    }

    public Vec2 plus(Vec2 v) {
        Vec2 vec = new Vec2(x + v.x, y + v.y);
        return vec;
    }

    public Vec2 minus(Vec2 v) {
        Vec2 vec = new Vec2(x - v.x, y - v.y);
        return vec;
    }

    public double skalarProdukt(Vec2 v) {
        double skalar = x * v.x + y * v.y;
        return skalar;
    }

    public double distanceTo(Vec2 v) {
        double dist = minus(v).length();
        return dist;
    }

    public Vec2 normalized(double newLength) {
        Vec2 vec = new Vec2(x, y);
        double factor = newLength / vec.length();
        vec = vec.mult(factor);
        return vec;
    }

}