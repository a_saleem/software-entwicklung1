import java.awt.Color;

public class Ball {
    Vec2 pos, vel;
    double rad = 0.2;
    Color col = new Color(255, 255, 255);

    public Ball(Vec2 pos, Vec2 vel, double rad, Color col) {
        this.pos = pos;
        this.vel = vel;
        this.rad = rad;
        this.col = col;
    }

    public void move(double deltaT) {
        this.pos = this.pos.plus(this.vel.mult(deltaT));
        // wenn vel gegen (0,0) geht, dann gibt diese Funktion irgendwann NaN aus.
        if (this.vel.length() < 0.1)
            this.vel = new Vec2(0, 0);
        else
            this.vel = this.vel.minus(this.vel.mult(20 * deltaT / this.vel.length()));
    }

    public void bump(Ball bump) {
        double skalarVelPos, laenge_x1Minusx2Quadrat;
        skalarVelPos = this.vel.minus(bump.vel).skalarProdukt(this.pos.minus(bump.pos));
        laenge_x1Minusx2Quadrat = this.pos.minus(bump.pos).length() * this.pos.minus(bump.pos).length();
        this.vel = this.vel.minus(this.pos.minus(bump.pos).mult(skalarVelPos / laenge_x1Minusx2Quadrat));
    }

    public void border(Spielfeld spielFeld) {
        if (this.pos.x - this.rad <= 0 || this.pos.x + this.rad >= spielFeld.getBreite()) {
            this.vel = new Vec2(-this.vel.x, this.vel.y);
        }
        if (this.pos.y - this.rad <= 0 || this.pos.y + this.rad >= spielFeld.getHoehe())
            this.vel = new Vec2(this.vel.x, -this.vel.y);
    }

    public boolean collision(Ball ball) {
        if (this.pos.distanceTo(ball.pos) <= this.rad + ball.rad) {
            Ball temp = new Ball(this.pos, this.vel, this.rad, this.col);
            this.bump(ball);
            ball.bump(temp);
            return true;
        }
        return false;
    }
}