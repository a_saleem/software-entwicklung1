import java.awt.Color;

public class Spielfeld {
    private int breite;
    private int hoehe;
    private Ball ball0, ball1, ball2;
    private int att, succ = 0;
    private boolean coll1, coll2, counted = false;

    public Spielfeld(int breite, int hoehe) {
        this.breite = breite;
        this.hoehe = hoehe;
        this.ball0 = new Ball(new Vec2(breite * 2 / 3, hoehe / 2), new Vec2(0, 0), 20, Color.white);
        this.ball1 = new Ball(new Vec2(breite / 3, hoehe / 3), new Vec2(0, 0), 20, Color.red);
        this.ball2 = new Ball(new Vec2(breite / 3, hoehe * 2 / 3), new Vec2(0, 0), 20, Color.red);
    }

    /**
     * Wird von GUI verwendet um die Breite des Spielfelds zu bekommen.
     * (darf nicht entfernt werden)
     */
    public int getBreite() {
        return breite;
    }

    /**
     * Wird von GUI verwendet um die Hoehe des Spielfelds zu bekommen.
     * (darf nicht entfernt werden)
     */
    public int getHoehe() {
        return hoehe;
    }

    /**
     * Wird von Gui mehrmals pro Sekunde aufgerufen
     * (darf nicht entfernt werden)
     *
     * @param deltaT die Zeit in Sekunden seit dem letzten Aufruf der Methode
     */
    public void simulationsSchritt(double deltaT) {
        ball0.move(deltaT);
        ball0.border(this);
        if (ball0.collision(ball1))
            coll1 = true;
        ball1.move(deltaT);
        ball1.border(this);
        ball1.collision(ball2);
        ball2.move(deltaT);
        ball2.border(this);
        if (ball0.collision(ball2))
            coll2 = true;
        if (coll1 && coll2 && !counted) {
            succ++;
            counted = true;
        }
    }

    /**
     * Wird von Gui immer nach simulationsSchritt aufgerufen um das Fenster zu zeichnen.
     * (darf nicht entfernt werden)
     *
     * @param seg ein Objekt zum zeichnen auf die Zeichenflaeche
     */
    public void zeichnen(SEGraphics seg) {

        // Zeichnen der Zeichenflaeche:
        seg.reset(new Color(14, 100, 80));
        // Zeichnen der Anzeige
        seg.drawString("Anzahl der Versuche: " + att, 20, 20, Color.white);
        seg.drawString("davon erfolgreich: " + succ, 20, 40, Color.white);
        // Zeichnen der drei Bälle
        if (coll1) seg.drawCircle(ball1.pos.x, ball1.pos.y, ball1.rad+1, Color.white);
        if (coll2) seg.drawCircle(ball2.pos.x, ball2.pos.y, ball2.rad+1, Color.white);
        seg.drawCircle(ball0.pos.x, ball0.pos.y, ball0.rad, ball0.col);
        seg.drawCircle(ball1.pos.x, ball1.pos.y, ball1.rad, ball1.col);
        seg.drawCircle(ball2.pos.x, ball2.pos.y, ball2.rad, ball2.col);
    }

    /**
     * Wird von Gui immer dann aufgerufen, wenn mit der Maus auf das Spielfeld geklickt wurde
     * (darf nicht entfernt werden)
     *
     * @param x die x-Koordinate des Klicks
     * @param y die y-Koordinate des Klicks
     */
    public void mouseClicked(int x, int y) {
        // Stehen die Bälle still?
        Vec2 click = new Vec2(x, y);
        if (ball0.vel.length() > 0 || ball1.vel.length() > 0 || ball2.vel.length() > 0)
            return;
        // Anstoss!    
        Ball bump = new Ball(click, ball0.pos.minus(click), 0, Color.white);
        ball0.bump(bump);
        att++;
        counted = false;
        coll1 = false;
        coll2 = false;
    }

}
