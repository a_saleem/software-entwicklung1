import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;

public class Gui extends JFrame implements ActionListener {
    private static final int TIME_STEP_MS = 10;

    private static final long serialVersionUID = 8320867273092453750L;

    private static Timer timer;
    private final DrawArea drawArea;
    private Spielfeld spielFeld;
    private long lastTime = System.currentTimeMillis();
    public boolean antiAliasing = true;
    private int tooSlowCount = 0;


    private Gui(Spielfeld spielfeld) {
        super("SE1-Carambolage");
        this.spielFeld = spielfeld;
        drawArea = new DrawArea();
        getContentPane().add(drawArea);
        pack();
        setResizable(false);
        setVisible(true);


        timer = new Timer(TIME_STEP_MS, this);
        timer.setInitialDelay(TIME_STEP_MS);
        timer.start();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        drawArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    spielFeld.mouseClicked(e.getX(), e.getY());
                }
            }
        });

    }


    public static void create(final Spielfeld spielfeld) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    new Gui(spielfeld);
                }
            });
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        long newTime = System.currentTimeMillis();
        double dt = (newTime - lastTime);
        if (dt > 3 * TIME_STEP_MS) {
            dt = 3 * TIME_STEP_MS;
            tooSlowCount++;
            if (tooSlowCount > 10) {
                antiAliasing = false;
            }
        } else {
            tooSlowCount = 0;
        }

        spielFeld.simulationsSchritt(dt / 1000.0);
        drawArea.repaint();
        lastTime = newTime;
    }

    class DrawArea extends Canvas {
        private static final long serialVersionUID = 6411839952303771403L;

        private SEGraphics seg;


        public DrawArea() {
            setPreferredSize(new Dimension(spielFeld.getBreite(), spielFeld.getHoehe()));
        }


        @Override
        public void paint(Graphics g) {
            BufferStrategy strategy = bufferStrategy();
            SEGraphics seg = graphics(strategy);
            spielFeld.zeichnen(seg);
            strategy.show();
            Toolkit.getDefaultToolkit().sync();
        }
        
        @Override
        public void update(Graphics g) {
        	paint(g);
        }

        private SEGraphics graphics(BufferStrategy strategy) {

            Graphics g = strategy.getDrawGraphics();
            Graphics2D g2 = (Graphics2D) g;
            if (antiAliasing) {
                RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2.setRenderingHints(rh);
            }

            seg = new SEGraphics(g2, spielFeld.getBreite(), spielFeld.getHoehe());
            return seg;
        }


        private BufferStrategy bufferStrategy() {
            BufferStrategy s = getBufferStrategy();
            if (s == null) {
                createBufferStrategy(20);
                s = getBufferStrategy();
            }
            return s;
        }
    }

}
