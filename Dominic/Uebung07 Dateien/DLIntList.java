public class DLIntList {
    private final DNode dummy;
    private int size = 0;

    /**
     * Erstellt eine neue, leere Liste
     */
    public DLIntList() {
        dummy = new DNode(null, 0, null);
        dummy.setNext(dummy);
        dummy.setPrev(dummy);
    }

    /**
     * Fuegt ein Element am Ende der Liste ein
     */
    public void add(int element) {
        DNode n = new DNode(dummy.getPrev(), element, dummy);
        dummy.getPrev().setNext(n);
        dummy.setPrev(n);
        size++;
    }

    /**
     * Liefert die Anzahl der Elemente in der Liste
     */
    public int size() {
        return size;
    }

    public void add(int index, int element) {
        // Aufgabe a)
        if (size < index) {
            return; //Liste ist kürzer als die gewünschte Position
        }
        DNode n = new DNode(dummy.getPrev(), element, dummy);
        // zusätzliches Element anhängen
        dummy.getPrev().setNext(n);
        dummy.setPrev(n);
        size++;
        // alle Elemente ab index um 1 nach rechts verschieben
        for (int i = size; i > index + 1; i--) {
            n.setValue(n.getPrev().getValue());
            n = n.getPrev();
        }
        // neuen Wert setzen
        n.setValue(element);
    }

    public int[] toArray() {
        // Aufgabe b)
        DNode n = dummy;
        int[] ar = new int[size];
        for (int i = 0; i < size; i++) {
            n = n.getNext();
            ar[i] = n.getValue();
        }
        return ar;
    }

    public int indexOf(int element) {
        // Aufgabe c)
        DNode n = dummy;
        for (int i = 0; i < size; i++) {
            n = n.getNext();
            if (element == n.getValue())
                return i;
        }
        return -1;
    }

    public int lastIndexOf(int element) {
        // Aufgabe d)
        DNode n = dummy;
        for (int i = size - 1; i >= 0; i--) {
            n = n.getPrev();
            if (element == n.getValue())
                return i;
        }
        return -1;
    }

    public void remove(int element) {
        // Aufgabe e)
        int x = indexOf(element);
        if (x == -1)
            return; // Element nicht vorhanden
        DNode n = dummy;
        for (int i = 0; i < x; i++) {
            n = n.getNext(); // Vorwärts bis vor das zu löschende Element
        }
        n.getNext().getNext().setPrev(n);
        n.setNext(n.getNext().getNext()); // ein Element überspringen
        size--;
        for (int i = x; i < size; i++) {
            n = n.getNext(); // Vorwärts bis vor das zu löschende Element
        }
    }

    public DLIntListRevIterator reverseIterator() {
        return new DLIntListRevIterator(this.dummy);
    }
}

class DLIntListRevIterator {
    private DNode current;
    private DNode dummy;

    public DLIntListRevIterator(DNode e) {
        dummy = e;
        current = dummy;   
    }

    public boolean hasNext() {
        return current.getPrev() != dummy; //prueft, ob es noch weitere Einträge gibt
    }

    public int next() {
        current = current.getPrev();
        int res = current.getValue();
        return res; //liefert den vorherigen Eintrag
    }
}

class Sum {
    static int sum(DLIntList list) {
        DLIntListRevIterator rev = list.reverseIterator();
        int sum = 0;
        for (int i = 0; i < list.size() && rev.hasNext(); i++)
            sum = sum + rev.next();
        return sum;
    }

}