import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Vec2Tests {


    public static final double DELTA = 0.01;

    @Test
    public void testLength() {
        assertEquals(0, new Vec2(0, 0).length(), DELTA);
        assertEquals(1, new Vec2(1, 0).length(), DELTA);
        assertEquals(2, new Vec2(0, 2).length(), DELTA);
        assertEquals(74, new Vec2(24, 70).length(), DELTA);
    }

    @Test
    public void testMult() {
        Vec2 v1 = new Vec2(4, -6);
        Vec2 v2 = v1.mult(0.5);
        assertEquals(4, v1.x, DELTA);
        assertEquals(-6, v1.y, DELTA);
        assertEquals(2, v2.x, DELTA);
        assertEquals(-3, v2.y, DELTA);
    }


    @Test
    public void testPlus() {
        Vec2 v1 = new Vec2(1, 2);
        Vec2 v2 = new Vec2(20, 10);
        Vec2 v3 = v1.plus(v2);
        assertEquals(21, v3.x, DELTA);
        assertEquals(12, v3.y, DELTA);
    }

    @Test
    public void testSkalarProdukt() {
        assertEquals(-2, new Vec2(1, 1).skalarProdukt(new Vec2(-1, -1)), DELTA);
        assertEquals(11, new Vec2(1, 2).skalarProdukt(new Vec2(3, 4)), DELTA);
    }

    @Test
    public void testDistanceTo() {
        assertEquals(2.8284, new Vec2(1, 1).distanceTo(new Vec2(-1, -1)), DELTA);
        assertEquals(5.0990, new Vec2(1, 4).distanceTo(new Vec2(2, 9)), DELTA);
    }

    @Test
    public void testNormalized() {
        Vec2 v1 = new Vec2(1, 1);
        assertEquals(0.707, v1.normalized(1).x, DELTA);
        assertEquals(0.707, v1.normalized(1).y, DELTA);
        Vec2 v2 = new Vec2(3, -5);
        assertEquals(5.145, v2.normalized(10).x, DELTA);
        assertEquals(-8.575, v2.normalized(10).y, DELTA);
    }
    
}
