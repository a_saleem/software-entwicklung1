import java.awt.Color;

public class Spielfeld {
    private int breite;
    private int hoehe;

    public Spielfeld(int breite, int hoehe) {
        this.breite = breite;
        this.hoehe = hoehe;
    }

    /**
     * Wird von GUI verwendet um die Breite des Spielfelds zu bekommen.
     * (darf nicht entfernt werden)
     */
    public int getBreite() {
        return breite;
    }

    /**
     * Wird von GUI verwendet um die Hoehe des Spielfelds zu bekommen.
     * (darf nicht entfernt werden)
     */
    public int getHoehe() {
        return hoehe;
    }

    /**
     * Wird von Gui mehrmals pro Sekunde aufgerufen
     * (darf nicht entfernt werden)
     *
     * @param deltaT die Zeit in Sekunden seit dem letzten Aufruf der Methode
     */
    public void simulationsSchritt(double deltaT) {
    }

    /**
     * Wird von Gui immer nach simulationsSchritt aufgerufen um das Fenster zu zeichnen.
     * (darf nicht entfernt werden)
     *
     * @param seg ein Objekt zum zeichnen auf die Zeichenflaeche
     */
    public void zeichnen(SEGraphics seg) {
        // Die folgenden Aufrufe dienen nur als Beispiel und sollten entfernt oder angepasst werden:

        // Beispiel-Aufruf zum zuruecksetzen der Zeichenflaeche:
        seg.reset(new Color(0, 0, 0));
        // Beispiel-Aufruf zum Zeichnen eines Kreises:
        seg.drawCircle(100, 100, 50, Color.RED);
    }

    /**
     * Wird von Gui immer dann aufgerufen, wenn mit der Maus auf das Spielfeld geklickt wurde
     * (darf nicht entfernt werden)
     *
     * @param x die x-Koordinate des Klicks
     * @param y die y-Koordinate des Klicks
     */
    public void mouseClicked(int x, int y) {
    }


}
