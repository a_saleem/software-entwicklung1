import org.junit.*;
import static org.junit.Assert.*;

public class DLIntListTests {
    @Test
    public void testAdd() {
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        int[] ar = list.toArray();
        for (int i = 0; i < ar.length; i++) {
            assertEquals(i + 1, ar[i]);
        }
    }

    @Test
    public void testAdd2() {
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        list.add(6, 66);
        int[] ar = list.toArray();
        for (int i = 0; i < ar.length; i++) {
            if (i == 6)
                assertEquals(66, ar[i]);
            else if (i > 6)
                assertEquals(i + 1 - 1, ar[i]);
            else
                assertEquals(i + 1, ar[i]);
        }
    }

    @Test
    public void testIndexOf() {
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        list.add(6, 66);
        list.add(7, 66);
        assertEquals(6, list.indexOf(66));
    }

    @Test
    public void testLastIndexOf() {
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        list.add(6, 66);
        list.add(7, 66);
        assertEquals(7, list.lastIndexOf(66));
    }

    @Test
    public void testRemove() {
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        list.add(7, 9);
        list.remove(9);
        list.remove(1);
        int[] ar = list.toArray();
        for (int i = 0; i < ar.length; i++) {
            StdOut.println(ar[i]);
        }

        for (int i = 0; i < ar.length; i++) {
            assertEquals(i + 2, ar[i]);
        }
    }

    @Test
    public void testRevIteratorNext() {
        int n = 10;
        DLIntList list = new DLIntList();
        for (int i = 1; i <= 10; i++)
            list.add(i);
        DLIntListRevIterator rev = list.reverseIterator();
        while (rev.hasNext() && n > 8) {
            rev.next();
            n--;
        }
        assertEquals(8, rev.next());
    }

    @Test
    public void testRevIteratorHasNext() {
        DLIntList list = new DLIntList();
        DLIntListRevIterator rev = list.reverseIterator();
        assertEquals(false, rev.hasNext());
    }

    @Test
    public void testSum() {
        DLIntList list = new DLIntList();
        for (int i = 1; i < 10; i++)
            list.add(i);
        assertEquals(45, Sum.sum(list));
    }

    @Test
    public void testSize() {
        DLIntList list = new DLIntList();
        for (int i = 1; i < 10; i++)
            list.add(i);
        list.remove(6);
        assertEquals(8, list.size());
    }

    @Test
    public void testNext2() {
        DLIntList list = new DLIntList();
        DLIntListRevIterator rev = list.reverseIterator();
        for (int i = 1; i < 10; i++)
            list.add(i);
        list.remove(6);
        if (rev.hasNext())
            rev.next();
        assertEquals(8, rev.next());
    }

    public void testGetNextGetPrev() {
        DNode dummy, n1, n2 = new DNode(null, 0, null);
        n1 = new DNode(null, 0, null);
        dummy = new DNode(n2, 0, n1);
        n1 = new DNode(dummy, 1, n2);
        n2 = new DNode(n1, 2, dummy);
        assertEquals(n1.getNext().getPrev().getValue(), n1.getValue());
        assertEquals(n2.getNext().getPrev().getValue(), n2.getValue());
        assertEquals(dummy.getNext().getPrev().getValue(), dummy.getValue());
    }
}
