public class ReadStrings {

    public static void main(String[] args) {
        //Test
        /*String[] Test = readStrings();
        for (int i=0; i<Test.length; i++)
        System.out.println(Test[i]);  */
    }

    public static String[] readStrings() {
        int n = 0; //Zählt die Eingaben
        String [] ar = new String[5];
        while (!StdIn.isEmpty()) { 
            if (n == ar.length) { //Ist das Array voll?               
                String [] artemp = new String[ar.length*2];
                for (int i=0; i<ar.length; i++) //kopiert das zu kleine Array in ein temporäres Array
                    artemp[i] = ar [i];
                ar = artemp;
            }
            ar[n]=StdIn.readString();      
            n++;   
            }
        String [] arfinal = new String[n];
        for (int i=0; i<n; i++) //kopiert das evtl. zu große Array in ein finales Array der Länge n
            arfinal[i] = ar [i];
        return arfinal;
    }

}