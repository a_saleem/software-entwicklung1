public class BarChart {
    public static void main(String[] args) {
        String [] eingabe = readStrings();
        double width;
        double gap;
        // Leere Eingabe --> autowidth
        if(args.length == 0) {
            width=autowidth(eingabe.length/2);
            gap=width*0.25;
        }  
        else {
            width = Double.parseDouble(args[0]);
            gap = Double.parseDouble(args[1]);
        }    
        //Abwechselnde Eingabe aufteilen in labels und heights
        String [] labels = new String[eingabe.length/2];
        double [] heights = new double[eingabe.length/2];
        for (int i=0; i<eingabe.length; i++){
            labels[i/2]=eingabe[i];
            i++;
            heights[(i-1)/2]=Double.parseDouble(eingabe[i]);
        }
        //Maximum ermitteln
        double max = max(heights);
        //Normalisieren
        double [] heightsnorm = normalisieren(heights, max);
        //Ausgabe
        //text(0.5,0.95, "Normalisiert mit Faktor: "+(Math.round(100/max*0.8))/100.0);
        drawBars(labels, heightsnorm, width, gap);
    }
    public static double autowidth(int anzahl){
        double width = anzahl+1;
        width = 1/width*0.8;
        return width;
    }
    public static double max (double [] heights){
        double max = 0;
        for (int i=0; i<heights.length; i++)
            if (heights[i]>max) max=heights[i];
        return max;
    }
    public static double [] normalisieren(double [] heights, double max){
        double [] heightsnorm = new double [heights.length];
        for (int i=0; i<heights.length; i++)
            heightsnorm[i]=heights[i]/max*0.8;
        return heightsnorm;
    }

    public static void text(double x, double y, String text){
        StdDraw.text(x, y, text);
    }

    public static void line(double x0, double y0, double x1, double y1){
        StdDraw.line(x0, y0, x1, y1);
    }

    public static void filledRectangle(double x, double y, double halfWidth, double halfHeight){
        StdDraw.filledRectangle(x,y,halfWidth, halfHeight);
    }

    public static void drawBar(double x, double width, String text, double height){
        text (x,0.05,text);
        line (0.05,0.1,0.95,0.1);
        filledRectangle (x, height/2+0.1, width/2, height/2);
    }

    public static void drawBars(String[] labels, double[] heights, double width, double gap){
        for (int i=0; i<labels.length; i++)
            drawBar((width+gap)*(i+1), width, labels[i], heights[i]);
    }

    public static String[] readStrings() {
        int n = 0; //Zählt die Eingaben
        String [] ar = new String[5];
        while (!StdIn.isEmpty()) { 
            if (n == ar.length) { //Ist das Array voll?               
                String [] artemp = new String[ar.length*2];
                for (int i=0; i<ar.length; i++) //kopiert das zu kleine Array in ein temporäres Array
                    artemp[i] = ar [i];
                ar = artemp;
            }
            ar[n]=StdIn.readString();      
            n++;   
            }
        String [] arfinal = new String[n];
        for (int i=0; i<n; i++) //kopiert das evtl. zu große Array in ein finales Array der Länge n
            arfinal[i] = ar [i];
        return arfinal;
    }
}
    