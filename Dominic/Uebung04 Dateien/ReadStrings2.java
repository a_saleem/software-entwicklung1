public class ReadStrings2 {

    public static void main(String[] args) {
        //Test
        String[] Test = readStrings();
        for (int i=0; i<Test.length; i++)
        System.out.println(Test[i]);  
    }

public static String[] readStrings() {
        int n = 0; //Zählt die Eingaben
        String [] ar = new String[5];
        String [] artemp = new String[10];
        boolean arvoll = false; //Ist das Array voll?
        while (!StdIn.isEmpty()) { 
            arvoll = (n == ar.length);
            if (arvoll) {                
                artemp = new String[ar.length*2];
                for (int i=0; i<ar.length; i++) //kopiert das zu kleine Array in ein temporäres Array
                    artemp[i] = ar [i];
                ar = new String[artemp.length];
                for (int i=0; i<ar.length; i++) //kopiert das temporäre Array zurück in das vergrößerte Array
                    ar[i] = artemp [i];
                }
            ar[n]=StdIn.readString();      
            n++;   
            }
        String [] arfinal = new String[n];
        for (int i=0; i<n; i++) //kopiert das evtl. zu große Array in ein finales Array der Länge n
            arfinal[i] = ar [i];
        return arfinal;
    }

}