import java.util.Arrays;

public class ArraysAndProcedures {

    public static void main(String[] args) {
        // Testen von Aufgabenteil a)
        int[] ar = {1, 2, 3, 4, 5};
        replaceAll(3, 42, ar);
        System.out.println(Arrays.toString(ar));
    }

    public static void replaceAll(int x, int y, int[] ar) {
        // TODO Aufgabe a)
    }

    public static void replaceFirst(int x, int y, int[] ar) {
        // TODO Aufgabe b)
    }

    public static void replaceLast(int x, int y, int[] ar) {
        // TODO Aufgabe b)
    }

    public static int[] substAll(int x, int y, int[] ar) {
        // TODO Aufgabe c)
        return null;
    }

    public static int[] onlyEven(int[] ar) {
        // TODO Aufgabe d)
        return null;
    }

    public static boolean allHaveZero(int[][] arrays) {
        // TODO Aufgabe e)
        return true;
    }

}