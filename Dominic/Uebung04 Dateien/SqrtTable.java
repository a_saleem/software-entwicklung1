public class SqrtTable {

    public static void main(String[] args) {
        while (!StdIn.isEmpty()) {
            double eingabe = StdIn.readDouble();
            StdOut.printf("%.2f,%.2f", eingabe, Math.sqrt(eingabe));
            System.out.println();
        }
    } 
}