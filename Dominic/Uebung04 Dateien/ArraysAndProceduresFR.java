import java.util.Arrays;

public class ArraysAndProcedures {

    public static void main(String[] args) {
		//Auswahl welches Prozedere zur Veränderung der Arrays genutzt werden soll
		int fall = Integer.parseInt(args[0]);
		if (fall < 1 || fall > 6) {
			System.out.println("Bitte geben Sie eine Zahl von 1 bis 6 an.");
		}
		//Testen der Aufgabenteile a) bis e)
		switch(fall) {
			case 1:
			int[] a1 = {1, 2, 3, 2, 4};
			replaceAll(2, 7, a1);
			System.out.println(Arrays.toString(a1));
			break;
				
			case 2:
			int[] a2 = {1, 2, 3, 2, 4};
			replaceFirst(2, 7, a2);
			System.out.println(Arrays.toString(a2));
			break;
			
			case 3:
			int[] a3 = {1, 2, 3, 2, 4};
			replaceLast(2, 7, a3);
			System.out.println(Arrays.toString(a3));
			break;
			
			case 4:
			int[] a4 = {1, 2, 3, 2, 4};
			int[] a42 = substAll(2, 7, a4);
			System.out.println(Arrays.toString(a4));
			System.out.println(Arrays.toString(a42));
			break;
			
			case 5:
			int[] ar = {1, 3, 2, 4, 7, 2};
			int[] arEven = onlyEven(ar);
			System.out.println(Arrays.toString(ar));
			System.out.println(Arrays.toString(arEven));
			break;
				
			case 6:
			int[][] arrays1 = {{1,2,0}, {0,7}, {7,0,7}};
			int[][] arrays2 = {{1,2,0}, {4,5,6}};
			System.out.println(allHaveZero(arrays1));
			System.out.println(allHaveZero(arrays2));
			break;
		}
    }

    public static void replaceAll(int x, int y, int[] a1) {
        // Aufgabe a)
        for (int i = 0; i < a1.length; i++) {
			if (a1[i] == x) {
				a1[i] = y;
			}
		}
	}

    public static void replaceFirst(int x, int y, int[] a2) {
        // Aufgabe b)
        for (int i = 0; i < a2.length; i++) {
			if (a2[i] == x) {
				a2[i] = y;
				break;
				}
			}
		}

    public static void replaceLast(int x, int y, int[] a3) {
        // Aufgabe b)
        for (int i = a3.length-1; i >= 0; i--) {
			if (a3[i] == x) {
				a3[i] = y;
				break;
			}
		}
    }

    public static int[] substAll(int x, int y, int[] a4) {
        // Aufgabe c)
        int [] a42 = new int[a4.length];
        for (int i = 0; i < a4.length; i++) {
			if (a4[i] == x) {
				a42[i] = y;
			} else {
				a42[i] = a4[i];
			}
		}
        return a42;
    }
		
    public static int[] onlyEven(int[] ar) {
        // Aufgabe d)
        int even = 0;
        for (int i = 0; i < ar.length; i++) {
			if (ar[i] % 2 == 0) {
				even = even +1;
			}
		}
		int [] arEven = new int[even];
		int j = 0;
		for (int i = 0; i < ar.length; i++) {
			if (ar[i] % 2 == 0) {
				arEven[j] = ar[i];
				j = j+1;
			}
		}
		return arEven;
	}

    public static boolean allHaveZero(int[][] arrays) {
        // Aufgabe e)
        boolean zero = true;
        //Spalte
        for (int i = 0; i < arrays.length && zero == true; i++) {
			zero = false;
			//Zeile
			for (int j = 0 ; j < arrays[i].length; j++) {  //effizienter mit && zero == false
				if (arrays[i][j] == 0) {
					zero = true;
				}
			}
		}
		return zero;
    }
}
