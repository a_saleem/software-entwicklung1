class HashEntry {
    int key;
    String data;
    HashEntry next;
    HashEntry(int key, String data) {
      this.key = key;
      this.data = data;
    }
  }
