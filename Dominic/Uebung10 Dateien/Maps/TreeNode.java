// Knoten fuer Binaerbaeume
class TreeNode {
    int key;
    String data;
    TreeNode left, right;

    public TreeNode(int key, String data) {
        this.key  = key;
        this.data = data;
    }
}
