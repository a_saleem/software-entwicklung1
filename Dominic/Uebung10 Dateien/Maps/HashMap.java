class HashMap implements Map {

  private HashEntry[] table;

  public HashMap( int tabsize ) {
    /* tabsize sollte eine Primzahl sein */
    this.table = new HashEntry[tabsize];
  }

  private int hash( int key ) {
    return Math.abs(key) % table.length;
  }

  public String get(int key) {
    // ermittle Index in der Hashtabelle
    int hix = hash(key);
    HashEntry entry = table[hix];

    while(entry != null) {
      // iteriere ueber die Elemente, bis Schluessel gefunden
      if (entry.key == key) {
        return entry.data;
      }
      entry = entry.next;
    }
    return null;
  }

  public void put (int key, String data) {
    if(data != null) {
      int hix = hash(key);
      HashEntry currentEntry = table[hix];
      HashEntry newEntry = new HashEntry(key, data);
      newEntry.next = currentEntry;  // fuege neuen Eintrag am Anfang ein
      table[hix] = newEntry;
    }
  }

  public void remove(int key) {
    int hix = hash(key);
    HashEntry entry = table[hix];

    // entferne alle HashEntries mit dem gegebenen Schlüssel
    HashEntry dummy = new HashEntry(0, null);
    dummy.next = entry;
    HashEntry current = dummy;
    while(current.next != null) {
      if(current.next.key == key) { // entferne den Eintrag
        current.next = current.next.next;
      } else {                    // gehe zum nächsten Eintrag
        current = current.next;
      }
    }
    table[hix] = dummy.next;
  }
}
