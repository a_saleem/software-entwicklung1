class LPMap implements Map {

    private HashEntry[] table;

    public LPMap(int tabsize) {
        /* tabsize sollte eine Primzahl sein */
        this.table = new HashEntry[tabsize];
    }

    private int hash(int key) {
        return Math.abs(key) % table.length;
    }

    public String get(int key) {
        // ermittle Index in der Hashtabelle
        int hix = hash(key);
        int n = 0;
        while (table[hix].key != key && n <= table.length) //Suche in der Tabelle nach dem Schlüssel
            if (hix < table.length - 1)
                hix++;
            else
                hix = 0;
        HashEntry entry = table[hix];
        if (table[hix].key == key)
            return entry.data;
        else
            return null;
    }

    public void put(int key, String data) {
        if (data != null) {
            int hix = hash(key);
            while (table[hix] != null && table[hix].key != key)
                if (hix < table.length - 1)
                    hix++;
                else
                    hix = 0;
            table[hix] = new HashEntry(key, data);
        }
    }

    public void remove(int key) {
        int hix = hash(key);
        HashEntry entry = table[hix];

        // entferne alle HashEntries mit dem gegebenen Schlüssel
        HashEntry dummy = new HashEntry(0, null);
        dummy.next = entry;
        HashEntry current = dummy;
        while (current.next != null) {
            if (current.next.key == key) { // entferne den Eintrag
                current.next = current.next.next;
            } else { // gehe zum nächsten Eintrag
                current = current.next;
            }
        }
        table[hix] = dummy.next;
    }
}
