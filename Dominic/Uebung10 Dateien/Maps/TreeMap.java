/** Repraesentiert einen sortierten markierten Binaerbaum
    Markierung entspricht dem Dataset */
class TreeMap implements Map {
    private TreeNode root;

    public String get(int key) {
        return get(root, key);
    }

    private String get(TreeNode node, int key) {
        if (node == null) {
            return null;
        }
        if (key < node.key) {
            // kleinere Elemente links suchen
            return get(node.left, key);
        } else if (key > node.key) {
            // groessere Elemente rechts suchen
            return get(node.right, key);
        } else {
            // gefunden!
            return node.data;
        }
    }

    /** fuegt ein Element dem Baum hinzu, falls kein Eintrag
        fuer den key vorhanden, sonst wird der Eintrag geupdated */
    public void put(int key, String data) {
        root = put(root, key, data);
    }

    private TreeNode put(TreeNode node, int key, String data){
        // Eintrag noch nicht vorhanden
        if (node == null) {
            return new TreeNode(key, data);
        } // Update von Eintrag
        if (key == node.key) {
            node.data = data;
        }
        else if (key < node.key) {
            node.left = put(node.left, key, data);
        } else {
            node.right = put(node.right, key, data);
        }
        return node;
    }



    /** entfernt ein Element aus dem Baum */
    public void remove(int key) {
        root = remove(root, key);
    }

    /** entfernt ein Element aus einem Teilbaum */
    private TreeNode remove(TreeNode node, int key) {
        if (node == null) {
            // aus leerem Teilbaum kann kein Element entfernt werden
            return null;
        }
        if (key < node.key) {
            // suche im linken Teilbaum
            TreeNode newLeft = remove(node.left, key);
            node.left = newLeft;
            return node;
        } else if (key > node.key) {
            // suche im rechten Teilbaum
            TreeNode newRight = remove(node.right, key);
            node.right = newRight;
            return node;
        } else {
            // zu loeschendes Element gefunden
            if (node.left == null) {
                // wenn der linke Teilbaum leer ist, dann nur den rechten nehmen
                return node.right;
            } else if (node.right == null) {
                // analog zum Fall node.left == null
                return node.left;
            } else {
                // wenn der zu löschende Knoten zwei Kinder hat, den Knoten mit dem groessten
                // Element aus dem linken Teilbaum suchen:
                TreeNode maxNodeLeft = maxNode(node.left);
                // Die Markierung von diesem Knoten nehmen wir fuer den aktuellen:
                node.key = maxNodeLeft.key;
                node.data = maxNodeLeft.data;
                // Und dann loeschen wir das Element aus dem linken Teilbaum:
                node.left = remove(node.left, node.key);
                return node;
            }
        }
    }

    /** liefert den Knoten mit der groessten Markierung im Teilbaum */
    private TreeNode maxNode(TreeNode node) {
        if (node.right == null) {
            return node;
        }
        return maxNode(node.right);
    }
}
