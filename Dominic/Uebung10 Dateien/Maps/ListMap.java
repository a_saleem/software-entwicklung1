import java.util.*;

class ListMap implements Map {
  private List<DataSet> elems;
  public ListMap() {
    elems = new ArrayList<DataSet>();
  }

  public String get (int key) {
    for (DataSet d : elems) {
      if (d.key == key) {
        return d.data;
      }
    }
    return null;
  }

  // Elemente koennen nur ueber Iterator waehrend des Iterierens entfernt werden!
  public void remove(int key) {
   Iterator<DataSet> iter = elems.iterator();
   while (iter.hasNext()) {
    DataSet d = iter.next();
    if (d.key == key) {
      iter.remove();
      return;
    }
   }
  }

  public void put(int key, String value) {
    for (DataSet d : elems) {
      if (d.key == key) {
        d.data = value;
        return;
      }
    }
    elems.add(new DataSet(key,value));
  }
}
