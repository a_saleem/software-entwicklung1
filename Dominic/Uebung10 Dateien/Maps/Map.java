// Schnittstelle für den abstrakten Datentyp Map
// Hier: int als Schlüssel und Strings als Werte
interface  Map {
   // Liefert ein String unter einem Schlüssel key
   String get(int key);
   // Fügt einen String value unter einem Schlüssel key ein
   void   put(int key, String value);
   // Entfernt den Eintrag zum Schlüssel key
   void   remove(int key);
}
