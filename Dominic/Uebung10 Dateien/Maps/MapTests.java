import static org.junit.Assert.assertEquals;
import org.junit.Test;

// Beispiele zur Verwendung von HashMap
public class MapTests {

  @Test
  public void arrayMapTest() {
    Map dict = new ListMap();
    mapTest(dict);
  }

  @Test
  public void treeMapTest() {
    Map dict = new TreeMap();
    mapTest(dict);
  }

  @Test
  public void hashMapTest() {
    Map dict = new HashMap(7);
    mapTest(dict);
  }

  void mapTest(Map dict) {
    // Array mit Testdaten
    int[] f = new int[5];
    f[0] = 5;
    f[1] = -4;
    f[2] = 2;
    f[3] = 17;
    f[4] = 0;


    // Einfuegen der Testdaten;
    // Zu Schluessel i wird das entsprechende Integer-Objekt als Wert eingefuegt
    for(int i = 0; i < f.length; i++ ) {
      dict.put( f[i], String.valueOf(f[i]) );
    }

    for (int i = 0; i < f.length; i++) {
      assertEquals(String.valueOf(f[i]), dict.get(f[i]));
    }

    // Update eines Eintrags
    dict.put(0, "25");
    assertEquals("25", dict.get(0));

    // Loeschen der Elemente
    for( int i = 0; i < f.length; i++ ) {
      dict.remove(f[i]);
      assertEquals(null,dict.get(f[i]));
    }
  }
}
