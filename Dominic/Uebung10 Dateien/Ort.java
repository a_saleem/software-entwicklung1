import java.util.*;

public class Ort {
    private final int postleitzahl;
    private final String name;

    public Ort(int postleitzahl, String name) {
        this.postleitzahl = postleitzahl;
        this.name = name;
    }

    public int getPostleitzahl() {
        return postleitzahl;
    }

    public String getName() {
        return name;
    }

    public int compare(Ort ort) {
        // Vergleich der Postleitzahl
        if (postleitzahl - ort.postleitzahl != 0)
            return postleitzahl - ort.postleitzahl;
        else // Vergleich der Ortsnamen
            return (name.compareTo(ort.name));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Ort))
            return false; // andere Klasse
        Ort ort = (Ort) obj; // Typkonvertierung
        return (postleitzahl == ort.postleitzahl) && name.equals(ort.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postleitzahl, name.length());
    }

    public static void main(String[] args) {
        Map<Ort, Integer> anzahlBewohner = new HashMap<>();
        Ort kaiserslautern = new Ort(67663, "Kaiserslautern");
        Ort kaiserslautern2 = new Ort(67663, "Kaiserslautern");
        Ort sembach = new Ort(67681, "Sembach");
        anzahlBewohner.put(kaiserslautern, 100000);
        anzahlBewohner.put(sembach, 500);
        System.out.println("Vergleich KL2, KL1: " + kaiserslautern2.compare(kaiserslautern));
        System.out.println("Equals KL2, KL1: " + kaiserslautern2.equals(kaiserslautern));
        System.out.println("Vergleich Sembach, KL1: " + sembach.compare(kaiserslautern));
        System.out.println("Equals Sembach, KL1: " + sembach.equals(kaiserslautern) + "\n");
        System.out.println("HashCode Kaiserslautern: " + kaiserslautern.hashCode());
        System.out.println("HashCode Kaiserslautern2: " + kaiserslautern2.hashCode());
        System.out.println("HashCode Sembach: " + sembach.hashCode() + "\n");
        System.out.println("Anzahl Bewohner KL1: " + anzahlBewohner.get(kaiserslautern));
        System.out.println("Anzahl Bewohner KL2: " + anzahlBewohner.get(kaiserslautern2));
        System.out.println("Anzahl Bewohner Sembach: " + anzahlBewohner.get(sembach));

    }

}