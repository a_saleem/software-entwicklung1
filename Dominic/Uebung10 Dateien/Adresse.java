import java.util.*;

public class Adresse {
    private final Ort ort;
    private final String strasse;
    private final String hausNummer;

    public Adresse(Ort ort, String strasse, String hausNummer) {
        this.ort = ort;
        this.strasse = strasse;
        this.hausNummer = hausNummer;
    }

    public Ort getOrt() {
        return ort;
    }

    public String getStrasse() {
        return strasse;
    }

    public String getHausNummer() {
        return hausNummer;
    }

    public int compare(Adresse adresse) {
        // Vergleich der Orte
        if (ort.compare(adresse.ort) != 0)
            return ort.compare(adresse.ort);
        else { // Vergleich der Straßen
            if (strasse.compareTo(adresse.strasse) != 0)
                return strasse.compareTo(adresse.strasse);
            else // Vergleich der Hausnummer
                return hausNummer.compareTo(adresse.hausNummer);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Adresse))
            return false; // andere Klasse
        Adresse adresse = (Adresse) obj; // Typkonvertierung
        return ort.equals(adresse.ort) && strasse.equals(adresse.strasse) && hausNummer.equals(adresse.hausNummer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ort.hashCode(), strasse.length(), hausNummer.length());
    }

    public static void main(String[] args) {
        Map<Adresse, Integer> anzahlBewohner = new TreeMap<>(new AdressVergleicher());
        Ort kaiserslautern = new Ort(67663, "Kaiserslautern");
        Adresse addr = new Adresse(kaiserslautern, "Gottlieb-Daimler-Str.", "34");
        Ort kaiserslautern2 = new Ort(67663, "Kaiserslautern");
        Adresse addr2 = new Adresse(kaiserslautern2, "Gottlieb-Daimler-Str.", "34");
        anzahlBewohner.put(addr, 23);
        anzahlBewohner.put(addr2, 2);

        System.out.println("Treemap:");
        System.out.println("Vergleich addr, addr2: " + addr2.compare(addr) + "\n");
        System.out.println("Anzahl Bewohner addr: " + anzahlBewohner.get(addr));
        System.out.println("Anzahl Bewohner addr2: " + anzahlBewohner.get(addr2));

        Map<Adresse, Integer> anzahlBewohnerHash = new HashMap<>();
        //Ort kaiserslautern = new Ort(67663, "Kaiserslautern");
        //Adresse addr = new Adresse(kaiserslautern, "Gottlieb-Daimler-Str.", "34");
        anzahlBewohnerHash.put(addr, 23);
        anzahlBewohnerHash.put(addr, 4);
        //assertEquals((Integer) 23, anzahlBewohner.get(addr));
        //Ort kaiserslautern2 = new Ort(67663, "Kaiserslautern");
        //Adresse addr2 = new Adresse(kaiserslautern2, "Gottlieb-Daimler-Str.", "34");
        //assertEquals((Integer) 23, anzahlBewohner.get(addr2));
        System.out.println("-----");
        System.out.println("Hashmap:");
        System.out.println("Equals addr, addr2: " + addr2.equals(addr) + "\n");
        System.out.println("HashCode addr: " + addr.hashCode());
        System.out.println("HashCode addr2: " + addr2.hashCode() + "\n");
        System.out.println("Anzahl Bewohner addr: " + anzahlBewohnerHash.get(addr));
        System.out.println("Anzahl Bewohner addr2: " + anzahlBewohnerHash.get(addr2));
    }
}