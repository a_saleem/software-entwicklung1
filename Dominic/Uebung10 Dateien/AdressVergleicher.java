import java.util.*;

public class AdressVergleicher implements Comparator<Adresse>{
    
    @Override
    public int compare(Adresse adresse1, Adresse adresse2){
        return adresse1.compare(adresse2);
    }
}