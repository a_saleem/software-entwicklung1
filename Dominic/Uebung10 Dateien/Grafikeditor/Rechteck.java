public class Rechteck extends Figur {
    private Vec2 start;
    private Vec2 linksOben;
    private double breite;
    private double hoehe;

    public Rechteck(Vec2 start, double breite, double hoehe) {
        this.start = start;
        this.linksOben = start;
        this.breite = breite;
        this.hoehe = hoehe;
    }

    @Override
    public void zeichnen(SEGraphics seg) {
        super.zeichnen(seg);
        seg.drawRect(linksOben.x, linksOben.y, breite, hoehe);
        //System.out.println("Start x: " + start.x + ", Start y: " + start.y);
        //System.out.println("LinksOben x: " + linksOben.x + ", LinksOben y: " + linksOben.y);
    }

    @Override
    public double abstandZu(Vec2 p) {
        Vec2 abstand = new Vec2(0, 0);
        int x, y;
        if (p.x > start.x) {
            x = (int) Math.max(p.x - breite - start.x, 0);
        } else {
            x = (int) Math.max(start.x - p.x, 0);
        }
        //System.out.println("x: "x);
        if (p.y > start.y) {
            y = (int) Math.max(p.y - hoehe - start.y, 0);
        } else {
            y = (int) Math.max(start.y - p.y, 0);
        }
        //System.out.println("y: "y);
        return new Vec2(x, y).length();
    }

    @Override
    public void verschiebenUm(Vec2 delta) {
        start = start.plus(delta);
        linksOben = linksOben.plus(delta);
    }

    public void setBreite(double x) {
        this.breite = x;
    }

    public void setHoehe(double y) {
        this.hoehe = y;
    }

    public void setLinksOben(double x, double y){
        this.linksOben = new Vec2(x,y);
    }

}
