public class Kreis extends Figur {
    private Vec2 mittelpunkt;
    private double radius;

    public Kreis(Vec2 mittelpunkt, double radius) {
        this.mittelpunkt = mittelpunkt;
        this.radius = radius;
    }

    @Override
    public void zeichnen(SEGraphics seg) {
        super.zeichnen(seg);
        seg.drawCircle(mittelpunkt.x, mittelpunkt.y, radius);

    }

    @Override
    public double abstandZu(Vec2 p) {
        return Math.max(p.distanceTo(mittelpunkt) - radius, 0);
    }

    @Override
    public void verschiebenUm(Vec2 delta) {
        mittelpunkt = mittelpunkt.plus(delta);
    }

    public void setRadius(Vec2 ende) {
        this.radius = mittelpunkt.distanceTo(ende);
    }

}
