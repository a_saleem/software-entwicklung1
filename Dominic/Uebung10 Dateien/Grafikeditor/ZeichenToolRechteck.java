public class ZeichenToolRechteck extends ZeichenTool {
    private Vec2 start;

    @Override
    public String getName() {
        return "Rechteck";
    }

    private Rechteck rechteck;

    @Override
    public void start(Vec2 pos) {
        start = pos;
        rechteck = new Rechteck(pos, 0, 0);
        zeichenBlatt.addFigur(rechteck);
    }

    @Override
    public void ziehen(Vec2 pos) {   
        rechteck.setBreite(Math.abs(pos.x - start.x));
        rechteck.setHoehe(Math.abs(pos.y - start.y));
        rechteck.setLinksOben(Math.min(pos.x, start.x), Math.min(pos.y, start.y));
    }

}
