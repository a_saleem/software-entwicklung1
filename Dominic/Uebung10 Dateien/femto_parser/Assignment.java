import java.util.Map;

public class Assignment extends Statement {
    private String var;
    private Expression exp;

    public Assignment(String var, Expression exp) {
        this.var = var;
        this.exp = exp;
    }

    public void eval(Map<String, Integer> context) {
        context.put(this.var, this.exp.eval(context));
    }

}
