import java.util.Map;

public class Variable extends Expression {
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
