import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class Program implements ASTNode {
    private List<Assignment> assignments;
    private Print printExp;
    private Map<String, Integer> context = new HashMap<>();

    public Program(List<Assignment> assignments, Print printExp) {
        this.assignments = assignments;
        this.printExp = printExp;
    }

    public void execute() {
        for (Assignment a : assignments) {
            a.eval(context);
        }
        //System.out.println(context.toString());
        printExp.getExp().eval(context);
        System.out.println(printExp.getExp().eval(printExp.getExp(), context));
    }

}
