import java.util.List;
import java.util.LinkedList;

public class FemtoParser extends Parser {

	Program Programm() {
		List<Assignment> assignments = VereinbarungsListe();
		accept(print);
		Expression exp = Ausdruck();
		accept(semikolon);
		return new Program(assignments, new Print(exp));
	}

	List<Assignment> VereinbarungsListe() {
		if (nextIs(typeInt)) {
			Assignment assgn = WertVereinbarung();
			List<Assignment> rest = VereinbarungsListe();
			rest.add(0, assgn);
			return rest;
		} else {
			// empty
			return new LinkedList<>();
		}
	}

	Assignment WertVereinbarung() {
		accept(typeInt);
		Parser.Token token = accept(bezeichner);
		accept(gleich);
		Expression exp = Ausdruck();
		accept(semikolon);
		return new Assignment(token.getText(), exp);
	}

	Expression Ausdruck() {
		if (nextIs(bezeichner)) {
			Parser.Token token = accept(bezeichner);
			return new Variable(token.getText());
		} else if (nextIs(zahl)) {
			Parser.Token token = accept(zahl);
			return new Constant(Integer.parseInt(token.getText()));
		} else {
			accept(klammerAuf);
			Expression exp1 = Ausdruck();
			Operator oper = Operator();
			Expression exp2 = Ausdruck();
			accept(klammerZu);
			switch (oper) {
				case ADD:
					return new Addition(exp1, exp2);
				case MULT:
					return new Multiplication(exp1, exp2);
				default:
					throw new RuntimeException("What else?");
			}
		}
	}

	Operator Operator() {
		if (nextIs(plus)) {
			accept(plus);
			return Operator.ADD;
		} else {
			accept(mult);
			return Operator.MULT;
		}
	}

	public static void main(String[] args) {
        FemtoParser p = new FemtoParser();
        Program prog = p.Programm();
        p.ende();
		prog.execute();
	}
}
