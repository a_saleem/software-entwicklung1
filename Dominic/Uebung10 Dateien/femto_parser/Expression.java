import java.util.Map;

public abstract class Expression implements ASTNode {

    public int eval(Map<String, Integer> context) {
        return eval(this, context);
    }

    public int eval(Expression e, Map<String, Integer> context) {
        if (e instanceof Multiplication) {
            Multiplication m = (Multiplication) e;
            return eval(m.getLeft(), context) * eval(m.getRight(), context);
        } else if (e instanceof Addition) {
            Addition a = (Addition) e;
            return eval(a.getLeft(), context) + eval(a.getRight(), context);
        } else if (e instanceof Variable) {
            Variable v = (Variable) e;
            return context.get(v.getName());
        } else if (e instanceof Constant) {
            Constant v = (Constant) e;
            return v.getValue();
        } else
            return 0;
    }

}
