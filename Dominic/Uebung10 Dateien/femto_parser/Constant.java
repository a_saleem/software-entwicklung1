import java.util.Map;

public class Constant extends Expression {
    private int value;

    public Constant(int value) {
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

}
