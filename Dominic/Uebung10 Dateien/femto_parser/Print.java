import java.util.Map;

public class Print extends Statement {
    private Expression exp;

    public Print(Expression exp) {
        this.exp = exp;
    }

    public Expression getExp() {
        return this.exp;
    }
}
