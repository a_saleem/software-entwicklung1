public class Intersection {
    /*
    requires a != null
    modifies \nothing
    ensures \result == (es existiert ein int i in [0,a.length-1], so dass a[i] == x)
    */
    public static boolean contains(int[] a, int x) {
        for (int i=0; i<a.length; i++)
            if (a[i] == x) return true;
        return false;
    }

    /*
    requires a != null
    modifies \nothing
    ensures \result == (fuer alle int i in [0,a.length-2]: a[i] < a[i+1])
    */
    public static boolean increasing(int[] a) {
        for (int i=0; i<a.length-1; i++)
            if (a[i] >= a[i+1]) return false;
        return true;
    }

    /*
    requires a != null
          && b != null
          && increasing(a)
          && increasing(b)
    modifies \nothing
    ensures \result == (fuer alle int x: !contains(a, x) || contains(b,x))
    */
    public static boolean subset(int[] a, int[] b) {
        boolean elementFound = true;
        for (int i=0; elementFound && i<a.length; i++) {
            elementFound = false;
            for (int j=0; !elementFound && j<b.length; j++) {
                if (a[i] == b[j]) elementFound = true;
            }
        }    
        return elementFound;
      }

    /*
    requires a != null
          && b != null
          && increasing(a)
          && increasing(b)
    modifies \nothing
    ensures \result != null
         && (fuer alle int x: (contains(a, x) && contains(b, x)) == contains(\result, x))
         && increasing(\result)
    */
    public static int[] intersection(int[] a, int[] b) {
        //Anzahl der gemeinsamen Elemente zählen
        int n = 0;
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<b.length; j++) {
                if (a[i] == b[j]) {
                    n++;
                    break;
                }    
            }
        }
        //Array schnittmenge anlegen   
        int [] schnittmenge = new int[n];
        n = 0;
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<b.length; j++) {
                if (a[i] == b[j]) {
                    schnittmenge[n]=a[i];
                    n++;
                    break;
                }    
            }
        }
        return schnittmenge;
    }
}