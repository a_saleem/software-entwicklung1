public class Superpowers {
    public static void main(String[] args) {
        // Werte von Eingabe lesen (1. Anzahl, danach jeweils a) DNA  b) Sequenz)
        String[] input = ReadStrings.readStrings();
        if (input.length < 3) {
            System.out.println("Keine Werte eingegeben.");
            return;
        }

        char[][] dna = dna(input); //DNA Werte aus Eingaben extrahieren
        char[][] seq = seq(input); // Sequenz-Werte aus Eingaben extrahieren
        // Anzahl der Übereinstimmungen pro Testfall ermitteln
        int[] amount = new int[Integer.parseInt(input[0])];
        for (int i = 0; i < Integer.parseInt(input[0]); i++) { //Anzahl Testfälle
            for (int j = 0; j < dna[i].length; j++) //Suche nach der Seq ab jedem Startpunkt der DNA
                amount[i] = amount(dna[i], seq[i], j, 0, amount[i]);
            //StdOut.println();
        }
        //Ausgabe
        output(amount);
    }

    //DNA Werte aus Eingaben extrahieren
    public static char[][] dna(String[] input) {
        char[][] dna = new char[Integer.parseInt(input[0])][];
        for (int i = 0; i < dna.length; i++) {
            dna[i] = input[i * 2 + 1].toCharArray();
        }
        return dna;
    }

    // Sequenz-Werte aus Eingaben extrahieren
    public static char[][] seq(String[] input) {
        char[][] seq = new char[Integer.parseInt(input[0])][];
        for (int i = 0; i < seq.length; i++) {
            seq[i] = input[i * 2 + 2].toCharArray();
        }
        return seq;
    }

    // Anzahl der Übereinstimmungen pro Testfall ermitteln
    public static int amount(char[] dna, char[] seq, int start, int matches, int amount) {
        // Sequenz vollstaendig gefunden?
        if (matches == seq.length) {
            amount++;
            matches = 0;
            return amount;
        }
        // Suche nach Seq
        if (start < dna.length) { //Ende erreicht?
            if (dna[start] == seq[matches] && (start < 10000) && (matches < 10000)) { //aktuelles Element gefunden stimmt überein?
                amount = amount(dna, seq, start + 1, matches + 1, amount); //nächstes Element überprüfen
            }
        }
    return amount;
    }

    //Ausgabe
    public static void output(int[] amount) {
        for (int i = 0; i < amount.length; i++) {
            System.out.print("Case #" + (i + 1) + ": ");
            System.out.println(amount[i]);
        }
    }
}