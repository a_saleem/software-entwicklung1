import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Arrays;

public class HistogramsTest {

  // Tests fuer values
  @Test
  public void valuesTestEven() {
    String[] ar = { "a", "1", "b", "2" };
    assertArrayEquals(new int[] { 1, 2 }, Histograms.values(ar));
  }

  @Test
  public void valuesTestOdd() {
    String[] ar = { "a", "100", "b", "200", "c" };
    assertArrayEquals(new int[] { 100, 200 }, Histograms.values(ar));
  }

  @Test
  public void valuesTestNegative() {
    String[] ar = { "a", "0", "b", "-1" };
    assertArrayEquals(new int[] { 0, -1 }, Histograms.values(ar));
  }

  //Tests fuer min
  @Test
  public void minTestPositive() {
    int[] ar = { 1, 2, 3 };
    assertEquals(1, Histograms.min(ar));
  }

  @Test
  public void minTestNegative() {
    int[] ar = { -10, 2, 3 };
    assertEquals(-10, Histograms.min(ar));
  }

  //Tests fuer max
  @Test
  public void maxTestPositive() {
    int[] ar = { 1, 2, 3 };
    assertEquals(3, Histograms.max(ar));
  }

  @Test
  public void maxTestNegative() {
    int[] ar = { -10, 2, 3 };
    assertEquals(3, Histograms.max(ar));
  }

  //Tests fuer counts
  @Test
  public void countsTestSingle() {
    int[] values = { 1, 2, 3 };
    assertArrayEquals(new int[] { 1, 1, 1 }, Histograms.counts(new String[] {"3"}, values, 1, 3));
  }

  @Test
  public void countsTestDouble() {
    int[] values = { 2, 2, 3 };
    assertArrayEquals(new int[] { 0, 2, 1 }, Histograms.counts(new String[] {"3"}, values, 1, 3));
  }

  @Test
  public void countsTestTriple() {
    int[] values = { 3, 3, 3 };
    assertArrayEquals(new int[] { 0, 0, 3 }, Histograms.counts(new String[] {"3"}, values, 1, 3));
  }
  
  @Test
  public void countsTestAllInOne() {
    int[] values = { 1, 2, 3 };
    assertArrayEquals(new int[] { 3 }, Histograms.counts(new String[] {"1"}, values, 1, 3));
  }

  //Tests fuer normalize
  @Test
  public void normalizeTest() {
    int[] ar1 = { 1, 2, 5 };
    int[] ar2 = { -1, 2, 5 };
    int[] ar3 = { 1, 2, 3 };
    assertArrayEquals(new int[] { 10, 20, 50 }, Histograms.normalize(ar1));
    assertArrayEquals(new int[] { -10, 20, 50 }, Histograms.normalize(ar2));
    assertArrayEquals(new int[] { 16, 33, 50 }, Histograms.normalize(ar3));
  }
}
