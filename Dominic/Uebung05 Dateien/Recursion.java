import java.util.Arrays;
public class Recursion {

    /*
	requires ar != null
		&& (0 <= left < ar.length)
		&& (0 <= right < ar.length)
    modifies a
    ensures im Bereich zwischen links und rechts werden zuerst alle Zahlen < pivot angeordnet, dann alle Zahlen <= pivot
    */
	static void partition(int[] a, int pivot, int left, int right) {
		if (left > right) {
			return;
		}
		if (a[left] < pivot) {
			partition(a, pivot, left+1, right);
		} else if (a[right] >= pivot) {
			partition(a, pivot, left, right-1);
		} else {
			int t = a[left];
			a[left] = a[right];
			a[right] = t;
			partition(a, pivot, left+1, right-1);
		}
	}

	/*
	requires i >= 0 && j >= 0
		&& n < a.length
		&& für alle i < a.length: m <= a[i].length
	*/
	static int sum(int[][] a, int i, int j, int n, int m) {
		if (j >= m || i > n) {
			return 0;
		} else if (i == n) {
			return sum(a, 0, j+1, n, m);
		} else {
			return a[i][j] + sum(a, i+1, j, n, m);
		}
	}

	public static void main(String...args) {
		int[] a = {7,2,3,10,11,5,3,2,23};
		partition(a, 10, 0, a.length-1);
		System.out.println(Arrays.toString(a));
		int[][] b = {{1},{1}};
		sum (b, 1, 1, 1, 1);
		System.out.println(Arrays.toString(b));
	}
}