public class Procs {
    /*
    requires ar != null
          && 0 <= n <= ar.length
    modifies \nothing
    ensures fuer alle int i in [0,n-1]: \result[i] == ar[i]
    */
    public static int[] take(int[] ar, int n) {
        int[] res = new int[n];
        for (int i=0; i<n; i++) {
            res[i] = ar[i];
        }        
        return res;
    }

    /*
    requires input != null
    modifies input
    ensures Die Reihenfolge aller Elemente von input[] wird umgekehrt (z.B. input[0] == \old[input.length-1], input[1] == \old[input.length-2])
    */
    public static void reverse(int[] input) {
        for (int i=0; i<input.length/2; i++) {
            int temp = input[i];
            input[i] = input[input.length - 1 - i];
            input[input.length - 1 - i] = temp;
        }
    }

    /*
    requires snippet != null
        && snippet.length > 0;
        && len >= 0;
    modifies \nothing
    ensures (fuer alle int i in [0,len-1]: \result[i] == snippet[i % snippet.length])
    */
    public static int[] repeat(int[] snippet, int len) {
        int[] res = new int[len];
        int resPos = 0;
        while (resPos < len) {
            for (int i=0; i < snippet.length && resPos < len; i++) {
                res[resPos] = snippet[i];
                resPos = resPos + 1;
            }
        }
        return res;
    }
}