public class Superpowers {
    public static void main(String[] args) {
        // Werte von Eingabe lesen (1. Anzahl, danach jeweils a) DNA  b) Sequenz)
        String[] input = ReadStrings.readStrings();
        if (input.length < 3) {
            System.out.println("Keine Werte eingegeben.");
            return;
        }

        String[] dna = dna(input);//DNA Werte aus Eingaben extrahieren
        String[] seq = seq(input);// Sequenz-Werte aus Eingaben extrahieren
        // Anzahl der Übereinstimmungen pro Testfall ermitteln
        int[] amount = new int[Integer.parseInt(input[0])];
        for (int i = 0; i < Integer.parseInt(input[0]); i++) { //Anzahl Testfälle
            amount[i] = amount(dna[i], seq[i]);
        }
        //Ausgabe
        output(amount);
    }

    //DNA Werte aus Eingaben extrahieren
    public static String[] dna(String[] input) {
        String[] dna = new String[Integer.parseInt(input[0])];
        for (int i = 0; i < dna.length; i++)
            dna[i] = input[i * 2 + 1];
        return dna;
    }

    // Sequenz-Werte aus Eingaben extrahieren
    public static String[] seq(String[] input) {
        String[] seq = new String[Integer.parseInt(input[0])];
        for (int i = 0; i < seq.length; i++)
            seq[i] = input[i * 2 + 2];
        return seq;
    }

    // Anzahl der Übereinstimmungen pro Testfall ermitteln
    public static int amount(String dna, String seq) {
        int amount = 0;
        for (int i = 0; i != -1;) {
            i = dna.indexOf(seq, i);
            if (i != -1) {
                amount++;
                i++;
            }
        }
        return amount;
    }

    //Ausgabe
    public static void output(int[] amount) {
        for (int i = 0; i < amount.length; i++) {
            System.out.print("Case #" + (i + 1) + ": ");
            System.out.println(amount[i]);
        }
    }
}