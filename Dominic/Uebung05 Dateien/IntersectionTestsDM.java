import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;

public class IntersectionTestsDM {
    @Test
    public void containsTest() {
        int [] a = {1, 2, 3};
        assertEquals("Ist 4 in {1, 2, 3} enthalten?", false, Intersection.contains(a, 4));
        assertEquals("Ist 3 in {1, 2, 3} enthalten?", true, Intersection.contains(a, 3));
    }

    @Test
    public void increasingTest() {
        int [] a1 = {1, 2, 3};
        int [] a2 = {1, 2, 2};
        assertEquals("Ist {1, 2, 3} aufsteigend?", true, Intersection.increasing(a1));
        assertEquals("Ist {1, 2, 3} aufsteigend?", false, Intersection.increasing(a2));
    }

    @Test
    public void subsetTest() {
        int [] leer = {};
        int [] a = {1, 2, 3, 4};
        int [] b1 = {2, 3, 4};
        int [] b2 = {2, 3, 4, 5};
        assertEquals("Wird true gezeigt, falls beide Arrays leer sind?", true, Intersection.subset(leer, leer));
        assertEquals("Ist {2, 3, 4} eine Teilmenge von {1, 2, 3, 4}?", true, Intersection.subset(b1, a));
        assertEquals("Ist {2, 3, 4, 5} eine Teilmenge von {1, 2, 3, 4}?", false, Intersection.subset(b2, a));
    }

    @Test
    public void intersectionTest() {
        int [] a = {2, 3, 4};
        int [] b1 = {5, 6};
        int [] b2 = {3, 4, 5};
        int [] erwartet1 = {};
        int [] erwartet2 = {3, 4};
        assertArrayEquals("Ist das Ergebnis != null?", erwartet1, Intersection.intersection(a, b1));
        assertArrayEquals("Wird die Schnittmenge {3, 4} korrekt ausgegeben?", erwartet2, Intersection.intersection(b2, a));           
    }
}