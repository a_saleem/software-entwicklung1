import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;

public class ProcTests {
    @Test
    public void takeTest() {
        int [] ar = {1, 2, 3};
        assertArrayEquals("{} als null Elemente von {1, 2, 3}?", new int [] {}, Procs.take(ar, 0));
        assertArrayEquals("{1, 2} als erste zwei Elemente von {1, 2, 3}?", new int [] {1, 2}, Procs.take(ar, 2));
        assertArrayEquals("{1, 2, 3} als erste drei Elemente von {1, 2, 3}?", new int [] {1, 2, 3}, Procs.take(ar, 3));
    }
    @Test
    public void reverseTest() {
        int [] ar = {1, 2};
        int [] ar1 = {1, 2, 3};
        int [] ar2 = {1};
        int [] leer = {};
        Procs.reverse(ar);
        assertArrayEquals("{2, 1} umgekehrt zu {1, 2}?", new int [] {2, 1}, ar);
        Procs.reverse(ar1);
        assertArrayEquals("{3, 2, 1} umgekehrt zu {1, 2, 3}?", new int [] {3, 2, 1}, ar1);
        Procs.reverse(ar2);
        assertArrayEquals("{1} bleibt {1}?", new int [] {1}, ar2);
        Procs.reverse(leer);
        assertArrayEquals("{} bleibt {}?", new int [] {}, leer);
    }
    @Test
    public void repeatTest() {
        int [] ar = {1, 2, 3};
        assertArrayEquals("{} als leeres Snippet von {1, 2, 3}?", new int [] {}, Procs.repeat(ar, 0));
        assertArrayEquals("{1, 2} als zweiteiliges Snippet von {1, 2, 3}?", new int [] {1, 2}, Procs.repeat(ar, 2));
        assertArrayEquals("{1, 2, 3} als dreiteiliges Snippet von {1, 2, 3}?", new int [] {1, 2, 3}, Procs.repeat(ar, 3));
        assertArrayEquals("{1, 2, 3, 1} als vierteiliges Snippet von {1, 2, 3}?", new int [] {1, 2, 3, 1}, Procs.repeat(ar, 4));
    }
}