public class Superpowers {
    public static void main(String[] args) {
        // Werte von Eingabe lesen (1. Anzahl, danach jeweils a) DNA  b) Sequenz)
        String[] input = ReadStrings.readStrings();
        if (input.length < 3) {
            System.out.println("Keine Werte eingegeben.");
            return;
        }

        char[][] dna = dna(input); //DNA Werte aus Eingaben extrahieren
        char[][] seq = seq(input); // Sequenz-Werte aus Eingaben extrahieren
        // Anzahl der Übereinstimmungen pro Testfall ermitteln
        int[] amount = new int[Integer.parseInt(input[0])];
        for (int i = 0; i < Integer.parseInt(input[0]); i++)
            amount[i] = amount(dna[i], seq[i]);
        //Ausgabe
        output(amount);

    }

    //DNA Werte aus Eingaben extrahieren
    public static char[][] dna(String[] input) {
        char[][] dna = new char[Integer.parseInt(input[0])][];
        for (int i = 0; i < dna.length; i++) {
            dna[i] = input[i * 2 + 1].toCharArray();
        }
        return dna;
    }

    // Sequenz-Werte aus Eingaben extrahieren
    public static char[][] seq(String[] input) {
        char[][] seq = new char[Integer.parseInt(input[0])][];
        for (int i = 0; i < seq.length; i++) {
            seq[i] = input[i * 2 + 2].toCharArray();
        }
        return seq;
    }

    // Anzahl der Übereinstimmungen pro Testfall ermitteln
    public static int amount(char[] dna, char[] seq) {
        int amount = 0;
        int matches;
        for (int i = 0; i < dna.length; i++) {
            matches = 0;
            for (int j = 0; j < seq.length && i < dna.length; j++)
                if (dna[i] != seq[j])
                    break;
                else {
                    matches++;
                    i++;
                    StdOut.print(seq[j]);
                    if (j==seq.length-1) StdOut.println();
                    if (matches == seq.length){
                        amount++;
                        StdOut.println();
                    }
                    if (matches > 3) StdOut.println(dna.length);
                }
        }
        return amount;
    }

    //Ausgabe
    public static void output(int[] amount) {
        for (int i = 0; i < amount.length; i++) {
            System.out.print("Case #" + (i + 1) + ": ");
            System.out.println(amount[i]);
        }
    }

}