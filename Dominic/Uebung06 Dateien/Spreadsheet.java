import java.util.Arrays;

public class Spreadsheet {
    int columns;
    int rows;
    int[][] values;
    Sum[][] sums;
    Sum init = new Sum(0, 0, 0, 0);

    /*
    public static void main(String[] args) {
        Spreadsheet sheet = new Spreadsheet(4, 4);
        sheet.set(1, 1, 1);
        sheet.set(1, 2, 2);
        sheet.setSumCalculation(1, 3, 1, 1, 1, 2);
    }
    */

    Spreadsheet(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
        this.values = new int[columns][rows];
        this.sums = new Sum[columns][rows];
        //sums initialisieren
        for (int i = 0; i < this.columns; i++)
            Arrays.fill(this.sums[i], init);
    }

    public class Sum {
        boolean isSum;
        int startColumn;
        int endColumn;
        int startRow;
        int endRow;

        Sum(int startColumn, int startRow, int endColumn, int endRow) {
            this.isSum = false; //Summe oder Festwert?
            this.startColumn = startColumn;
            this.endColumn = endColumn;
            this.startRow = startRow;
            this.endRow = endRow;
        }
    }

    public void set(int column, int row, int value) {
        values[column][row] = value; //Wert setzen
        sums[column][row].isSum = false; //Keine Summe, sondern Festwert
        refresh(column, row); //nach jeder Änderung Summenfelder aktualisieren
    }

    public int get(int column, int row) {
        return values[column][row];
    }

    public void setSumCalculation(int resultColumn, int resultRow, int startColumn, int startRow, int endColumn,
            int endRow) {
        int result = 0;
        // result berechnen
        for (int i = startColumn; i <= endColumn; i++)
            for (int j = startRow; j <= endRow; j++)
                result = result + get(i, j);
        values[resultColumn][resultRow] = result;
        // Summenbezüge festhalten
        sums[resultColumn][resultRow] = new Sum(startColumn, startRow, endColumn, endRow);
        sums[resultColumn][resultRow].isSum = true;
        //nach jeder Änderung Summenfelder aktualisieren
        refresh(resultColumn, resultRow);
    }

    public void refresh(int startColumn, int startRow) {
        for (int i = startColumn; i < columns; i++)
            for (int j = startRow + 1; j < rows; j++) //Aktualisierung beginnt 1 Feld nach der letzten Änderung
                if (sums[i][j].isSum == true)
                    setSumCalculation(i, j, sums[i][j].startColumn, sums[i][j].startRow, sums[i][j].endColumn,
                            sums[i][j].endRow);
    }
}