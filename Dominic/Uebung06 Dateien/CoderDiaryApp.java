public class CoderDiaryApp {

    public static void main(String[] args) {
        // Abfrage des Namens
        StdOut.print("Name: ");
        String name = StdIn.readLine();

        // Erstellen eines CoderDiaries
        CoderDiary d = new CoderDiary(name);

        // Interaktionsschleife
        boolean exit = false;
        do {
            StdOut.print("Aktion (exit, add, remove, show, loc, speed): ");
            String action = StdIn.readString();
            switch (action) {
             case "exit":    exit = true; break;
             case "add" :    Entry e = readEntry(); d.add(e); break;
             case "remove" : int i = StdIn.readInt(); d.remove(i); break;
             case "show":    StdOut.println(d);  break;
             case "speed" :  StdOut.println(d.dayWithMaxSpeed()); break;
             case "loc" :    StdOut.println(d.totalLoc()); break;
             default:        StdOut.println("Aktion nicht bekannt!");
            }
        } while(!exit);
    }

    // Einlesen der Daten für einen neuen Eintrag
    public static Entry readEntry() {
        StdOut.print("Datum (dd.mm.yyyy): ");
        String date = StdIn.readString();
        StdOut.print("LOC:                ");
        int loc = StdIn.readInt();
        StdOut.print("Zeit:               ");
        int time = StdIn.readInt();
        StdOut.print("Kommentar:          ");
        String comment = StdIn.readString();
        return new Entry(new Date(date), loc, time, comment);
    }
}
