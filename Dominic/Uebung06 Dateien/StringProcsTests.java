import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringProcsTests {

    @Test
    public void testExtension1() {
        assertEquals("jpg", StringProcs.extension("katze.jpg"));
    }

    @Test
    public void testExtension2() {
        assertEquals("zip", StringProcs.extension("meine.bilder.zip"));
    }

    @Test
    public void testExtension_empty() {
        assertEquals("", StringProcs.extension("hund"));
    }

    @Test
    public void testSwitchCommaAndDot1() {
        assertEquals("3.14", StringProcs.switchCommaAndDot("3,14"));
    }

    @Test
    public void testSwitchCommaAndDot2() {
        assertEquals("3,14", StringProcs.switchCommaAndDot("3.14"));
    }

    @Test
    public void testSwitchCommaAndDot3() {
        assertEquals("1,500,000.00", StringProcs.switchCommaAndDot("1.500.000,00"));
    }

    @Test
    public void testLetterCount1() {
        assertEquals(3, StringProcs.letterCount("abc"));
    }

    @Test
    public void testLetterCount2() {
        assertEquals(0, StringProcs.letterCount(""));
    }

    @Test
    public void testLetterCount3() {
        assertEquals(9, StringProcs.letterCount("abc de fghi"));
    }

    @Test
    public void testWordCount1() {
        assertEquals(0, StringProcs.wordCount(""));
    }

    @Test
    public void testWordCount2() {
        assertEquals(1, StringProcs.wordCount("Hallo"));
    }

    @Test
    public void testWordCount3() {
        assertEquals(5, StringProcs.wordCount("abc d efg hij klm"));
    }

    @Test
    public void testWordCount4() {
        assertEquals(2, StringProcs.wordCount("...a..b"));
    }

    @Test
    public void testWordCount5() {
        assertEquals(2, StringProcs.wordCount("a..b......"));
    }


}