public class Aufzugsteuerung {
    int currentFloor;
    int minFloor;
    int maxFloor;
    boolean open = true;
    IntQueue queue;
    // a) Konstruktor
    Aufzugsteuerung(int currentFloor, int minFloor, int maxFloor) {
        this.currentFloor = currentFloor;
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        this.open = true;
        this.queue = new IntQueue();
    }

    // b) Methode Request
    public void request(int floor) {
        this.queue.enqueue(floor);
    }

    // c) Methode Action
    public int action() {
        if (this.queue.isEmpty())
            return 0;
        else {
            //next == aktuelles Stockwerk && Tür zu?
            if (this.queue.peek() == this.currentFloor && !this.open) {
                this.queue.dequeue();
                this.open = true;
                return 3; // Tür öffnen
            //Tür offen?
            } else if (this.open) {
                this.open = false;
                return 4; //Tür schließen
            //next > aktuelles Stockwerk?
            } else if (this.queue.peek() > this.currentFloor && !this.open) {
                this.currentFloor++;
                return 1; // ein Stockwerk nach oben
            //next < aktuelles Stockwerk!
            } else  {
                this.currentFloor--;
                return 2; // ein Stockwerk nach unten
            }
        }
    }
}