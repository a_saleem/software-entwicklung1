public class Aufzugsteuerung2 {
    int currentFloor;
    int minFloor;
    int maxFloor;
    boolean open;
    IntQueue queue;

    // a) Konstruktor
    Aufzugsteuerung(int currentFloor, int minFloor, int maxFloor) {
        this.currentFloor = currentFloor;
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        this.open = true;
        this.queue = new IntQueue();
    }

    // b) Methode Request
    public void request(int floor) {
        queue.enqueue(floor);
    }

    // c) Methode Action
    public int action() {
        if (queue.isEmpty())
            return 0;
        else {
            //next == aktuelles Stockwerk && Tür zu?
            if (queue.peek() == currentFloor && !open) {
                queue.dequeue();
                open = true;
                return 3; // Tür öffnen
            //Tür offen?
            } else if (open) {
                open = false;
                return 4; //Tür schließen
            //next > aktuelles Stockwerk?
            } else if (queue.peek() > currentFloor && !open) {
                currentFloor++;
                return 1; // ein Stockwerk nach oben
            //next < aktuelles Stockwerk!
            } else  {
                currentFloor--;
                return 2; // ein Stockwerk nach unten
            }
        }
    }
}