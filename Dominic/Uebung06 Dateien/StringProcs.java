public class StringProcs {
    public static void main(String[] args) {

    }

    public static String extension(String f) {
        if (f.contains(".")) {
            f = f.substring(f.lastIndexOf(".")+1, f.length());
            return f;
        }
        return "";
    }

    public static String switchCommaAndDot(String s) {
        char[] ar = s.toCharArray();
        for (int i = 0; i < ar.length; i++)
            if (ar[i] == '.')
                ar[i] = ',';
            else if (ar[i] == ',')
                ar[i] = '.';
        s = new String(ar);
        return s;
    }

    public static int letterCount(String s) {
        char[] ar = s.toCharArray();
        int n = 0;
        for (int i = 0; i < ar.length; i++)
            if (Character.isLetter(ar[i]))
                n++;
        return n;
    }

    public static int wordCount(String s) {
        char[] ar = s.toCharArray();
        int n = 0;
        for (int i = 0; i < ar.length; i++)
            if (Character.isLetter(ar[i])) {
                n++;
                while (Character.isLetter(ar[i]) && i < ar.length - 1)
                    i++;
            }
        return n;
    }

}