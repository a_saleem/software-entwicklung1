import java.awt.Color;

public class ColorSquares {
    public static void main(String[] args) {
        int r = Integer.parseInt(args[0]);
        int g = Integer.parseInt(args[1]);
        int b = Integer.parseInt(args[2]);

        Color c1 = new Color(r, g, b);
        Color c2 = c1.brighter();
        Color c3 = c1.darker();

        StdDraw.setPenColor(c3);
        StdDraw.filledSquare(.25, .5, .3);

        StdDraw.setPenColor(c1);
        StdDraw.filledSquare(.25, .5, .2);

        StdDraw.setPenColor(c2);
        StdDraw.filledSquare(.25, .5, .1);

    }
}