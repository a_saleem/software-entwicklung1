import java.awt.Color;

public class PicEdit {
    public static void main(String[] args) {
        // Interaktionsschleife
        boolean exit = false;
        Picture pic;
        do {
            StdOut.println();
            StdOut.print("Aktion (e(x)it, (l)uminance, (s)aturation, (r)otate): ");
            String action = StdIn.readString();
            switch (action) {
            case "exit":
            case "x":
                exit = true;
                break;
            case "luminance":
            case "l":
                StdOut.println();
                StdOut.print("Dateiname: ");
                pic = new Picture(StdIn.readString());
                StdOut.println("Durchschnittliche Luminanz: " + averageLum(pic));
                break;
            case "saturation":
            case "s":
                StdOut.println();
                StdOut.print("Name der Eingabedatei: ");
                pic = new Picture(StdIn.readString());
                StdOut.print("Änderung der Sättigung: ");
                double change = StdIn.readDouble();
                StdOut.println("Bitte warten...");
                pic = changeSaturation(pic, change);
                StdOut.println(" ...fertig.");
                StdOut.print("Name der Ausgabedatei: ");
                pic.save(StdIn.readString());
                break;
            case "rotate":
            case "r":
                StdOut.println();
                StdOut.print("Name der Eingabedatei: ");
                pic = new Picture(StdIn.readString());
                StdOut.print("Bitte warten...");
                pic = rotatedLeft(pic);
                StdOut.println(" ...fertig.");
                StdOut.print("Name der Ausgabedatei: ");
                pic.save(StdIn.readString());
                break;
            default:
                StdOut.println("Aktion nicht bekannt!");
            }
        } while (!exit);
    }

    //Berechnung der Schwarz-Weiß-Luminanz einer Farbe c
    public static double lum(Color c) {
        double lum = 0.299 * c.getRed() + 0.587 * c.getGreen() + 0.114 * c.getBlue();
        return lum;
    }

    //Berechnung der durchschnittlichen Schwarz-Weiß-Luminanz eines Bildes pic
    public static double averageLum(Picture pic) {
        double lum = 0;
        for (int i = 0; i < pic.width(); i++)
            for (int j = 0; j < pic.height(); j++)
                lum = lum + lum(pic.get(i, j));
        lum = Math.round(10 * lum / (pic.width() * pic.height())) / 10.0;
        return lum;
    }

    //Veränderung der Sättigung einer Farbe c um den Wert change
    public static Color changedColorSaturation(Color c, double change) {
        int r = c.getRed();
        int g = c.getGreen();
        int b = c.getBlue();
        double lum = lum(c);
        r = (int) Math.round(Math.min(Math.max(lum + (r - lum) * change, 0), 255));
        g = (int) Math.round(Math.min(Math.max(lum + (g - lum) * change, 0), 255));
        b = (int) Math.round(Math.min(Math.max(lum + (b - lum) * change, 0), 255));
        c = new Color(r, g, b);
        return c;
    }

    //Veränderung der Sättigung eines Bildes pic um den Wert change
    public static Picture changeSaturation(Picture pic, double change) {
        Color[][] c = new Color[pic.width()][pic.height()];
        for (int i = 0; i < pic.width(); i++)
            for (int j = 0; j < pic.height(); j++) {
                c[i][j] = pic.get(i, j);
                c[i][j] = changedColorSaturation(c[i][j], change);
                pic.set(i, j, c[i][j]);
            }
        return pic;
    }

    //Drehung des Bildes pic um 90 Grad nach links
    public static Picture rotatedLeft(Picture pic) {
        Picture rotated = new Picture(pic.height(), pic.width());
        for (int h = 0; h < pic.height(); h++)
            for (int w = 0; w < pic.width(); w++)
                rotated.set(h, w, pic.get(pic.width() - 1 - w, h));
        return rotated;
    }
}