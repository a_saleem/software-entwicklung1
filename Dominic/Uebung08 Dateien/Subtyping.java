interface A {
  String m();
}

interface B {
  String m();
}

interface C extends A {
  String p();
}

interface D extends A, B {
  String q();
}

class E implements B {
  public String m() {
    return "Em";
  }

  public String r() {
    return "Er";
  }
}

class F implements C, D {
  public String m() {
    return "Fm";
  }

  public String p() {
    return "Fp";
  }

  public String q() {
    return "Fq";
  }

  public String r() {
    return "Fr";
  }
}

public class Subtyping {
  public static void main(String[] args) {

    // ... §\label{mainblock}§

    //a) --> Interface A nicht verinstanzierbar
    /*
    A x = new A();
    System.out.println(x.m());
    */

    //b) --> "Er"
    /*
    E x = new E();
    System.out.println(x.r());
    */

    //c) --> "Fm"
    /*
    A x = new F();
    System.out.println(x.m());
    */

    //d) --> r() ist in Interface A nicht bekannt
    /*
    A x = new F();
    System.out.println(x.r());
    */

    //e) --> r() ist in Interface B nicht bekannt
    /*
    B x = new E();
    B y = new F();
    System.out.println(x.r() + y.r());
    */

    //f) --> "EmFm"
    /*
    B x = new E();
    B y = new F();
    System.out.println(x.m() + y.m());
    */

    //g) Typ A und B haben keine Subtyp-Beziehung
    /*
    B x = new F();
    A y = x;
    System.out.println(x.m() + y.m());
    */

    //h) Methode p() nicht in Typ D oder seinen Supertypen A und B bekannt
    /*
    D x = new F();
    System.out.println(x.m() + x.p());
    */

  }
}