interface Figur {
	double flaeche();
}

class Figuren {

	public static double flaeche(Figur[] figuren) {
		double a = 0;
		for (Figur f : figuren)
			a = a + f.flaeche();
		return a;
	}
}

class Rechteck implements Figur {
	private final double x;
	private final double y;
	private final double width;
	private final double height;

	Rechteck(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public double flaeche() {
		return width * height;
	}
}

class Kreis implements Figur {
	private final double x;
	private final double y;
	private final double radius;

	Kreis(double x, double y, double radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public double flaeche() {
		return Math.PI * radius * radius;
	}
}