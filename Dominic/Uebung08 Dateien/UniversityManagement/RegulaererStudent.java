class RegulaererStudent implements Student { 
  private int matrikel;
  private String name;
  private Date birthdate;

  RegulaererStudent(String name, int matrikel, Date birthdate) {
    this.name = name;
    this.matrikel = matrikel;
    this.birthdate = birthdate;
  }

  public String getName() {
    return this.name;
  }

  public int getMatrikel() {
    return this.matrikel;
  }

  public Date getBirthdate() {
    return this.birthdate;
  }

  public void print() {
    System.out.println("Student: " + this.name 
        + ", Matr. " + this.matrikel + ", geb. " + this.birthdate);
  }

}