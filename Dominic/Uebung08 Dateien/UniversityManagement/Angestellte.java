interface Angestellte extends Person, Printable {
    String getAbteilung();
}