// Klassentyp fuer Datumswerte
// Besteht aus Tag, Jahr und Monat
class Date {
  int day;   
  int month; 
  int year; 

  Date(int day, int month, int year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  // String-Repraesentation
  // Beispiel: "01.12.2016"
  public String toString() {
    return this.day + "." + this.month + "." + this.year;
  }
}