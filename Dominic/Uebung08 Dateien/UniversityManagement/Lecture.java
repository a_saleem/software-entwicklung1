import java.util.List;
import java.util.ArrayList;

class Lecture implements Course {
  private String title;
  private String lecturer;
  private List<Student> students;

  Lecture(String title, String lecturer) {
    this.title = title;
    this.lecturer = lecturer;
    this.students = new ArrayList<Student>();
  }

  public String getTitel() {
    return title;
  }
  public String getLecturer() {
    return lecturer;
  }
  public boolean enroll(Student s) {
    return students.add(s);
  }
  public void printStudents() {
    for(Student s : students) {
      System.out.println(s);
    }
  }
}
