interface Student extends Person, Printable {
    int getMatrikel();
}