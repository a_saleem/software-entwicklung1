interface University {
      Student register(String name);
      void deregister(int matrikel);
      Course findCourse(String courseTitle);
      void addCourse(Course c);
      void printStudents();
}