import java.util.List;
import java.util.LinkedList;

class Seminar implements Course {
  private String title;
  private int capacity;
  private List<Student> students;

  Seminar(String title, int capacity) {
    this.title = title;
    this.capacity = capacity;
    this.students = new ArrayList<Student>();
  }
  public String getTitel() {
    return title;
  }
  public boolean enroll(Student s) {
    if (students.size() < capacity) {
      students.add(s);
      return true;
    }
    return false;
  }
  public void printStudents() {
    for(Student s : students) {
      System.out.println(s);
    }
  }
}
