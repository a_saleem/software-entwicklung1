class WissAngestellte implements Angestellte { 
  private String arbeitsgruppe;
  private String abteilung;
  private String name;
  private Date birthdate;

  WissAngestellte(String name, String arbeitsgruppe, Date birthdate, String abteilung) {
    this.name = name;
    this.arbeitsgruppe = arbeitsgruppe;
    this.birthdate = birthdate;
    this.abteilung = abteilung;
  }

  public String getName() {
    return this.name;
  }

  public String getArbeitsgruppe() {
    return this.arbeitsgruppe;
  }

  public String getAbteilung() {
    return this.abteilung;
  }

  public Date getBirthdate() {
    return this.birthdate;
  }

  public void print() {
    System.out.println("Wiss. Angestellte: " + this.name 
        + ", AG " + this.arbeitsgruppe + 
        ", Abteilung " + this.abteilung + ", geb. " + this.birthdate);
  }

 }