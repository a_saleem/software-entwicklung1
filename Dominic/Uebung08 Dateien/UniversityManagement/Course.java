interface Course {
  String getTitel();
  boolean enroll(Student s);
  void printStudents();
}