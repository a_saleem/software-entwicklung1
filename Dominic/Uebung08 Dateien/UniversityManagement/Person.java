interface Person {
  String getName();
  Date getBirthdate();
}