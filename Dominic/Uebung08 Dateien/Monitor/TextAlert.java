import java.util.Locale;

public class TextAlert implements Alert {
    private String message;

    TextAlert(String message) {
        this.message = message;
    }

    public void startAlert(Dataset dataset) {
        String printmessage = message.replace("%t", "%d");
        printmessage = printmessage.replace("%v", "%.2f");
        System.out.printf(Locale.US, printmessage + "\n", dataset.getZeit(), dataset.getWert());
    }

}