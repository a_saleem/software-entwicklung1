import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

interface Trigger{
    boolean isTriggered(List<Dataset> data);
}