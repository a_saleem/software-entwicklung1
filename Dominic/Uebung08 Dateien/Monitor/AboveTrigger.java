import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class AboveTrigger implements Trigger {
    private double bound;
    private boolean set;

    AboveTrigger(double bound) {
        this.bound = bound;
        this.set = false;
    }

    public void Set() {
        this.set = true;
    }

    public boolean isSet() {
        return this.set;
    }

    public boolean isTriggered(List<Dataset> data) {
        Dataset dataset = data.get(data.size()-1);
        return (this.getBound() < dataset.getWert());
    }

    public double getBound() {
        return this.bound;
    }
}