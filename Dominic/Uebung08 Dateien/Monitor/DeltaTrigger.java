import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class DeltaTrigger implements Trigger {
    private long t;
    private double maxChange;
    private boolean set = false;

    DeltaTrigger(long t, double maxChange) {
        this.t = t;
        this.maxChange = maxChange;
        this.set = false;
    }

    public void Set() {
        this.set = true;
    }

    public boolean isSet() {
        return this.set;
    }

    /*public void addTrigger(DeltaTrigger delta){
        this.t = delta.t;
        this.maxChange = delta.maxChange;
    }*/

    public boolean isTriggered(List<Dataset> data) {
        Dataset dataset = data.get(data.size() - 1);
        double min = dataset.getWert();
        double max = dataset.getWert();
        long deltaStart = dataset.getZeit() - this.t;
        int i = 1;
        while (dataset.getZeit() > deltaStart && i <= data.size()) {
            dataset = data.get(data.size() - i);
            if (dataset.getWert() > max)
                max = dataset.getWert();
            if (dataset.getWert() < min)
                min = dataset.getWert();
            i++;
        }
        return (max - min > this.maxChange);
    }

    public long getZeit() {
        return this.t;
    }

    public double getChange() {
        return this.maxChange;
    }
}