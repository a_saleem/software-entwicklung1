public class Main {

    public static void main(String args[]) {
        Monitor m = new Monitor();
        StdOut.println((double) 3);
        
        m.addTrigger(new DeltaTrigger(10000, 10));
        m.addAlert(new TextAlert("%t ms: Value changed to %v"));
        m.report(new Dataset(0, 50));
        m.startAlerts();
        m.report(new Dataset(1000, 51));
        m.report(new Dataset(2000, 52));
        m.addAlert(new EmailAlert("@@","%t ms: Value changed to %v"));
        m.startAlerts();
    }
}