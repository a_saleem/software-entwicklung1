import java.util.Locale;

public class EmailAlert implements Alert{
    private String email;
    private String message;
    EmailAlert(String email, String message){
        this.email = email;
        this.message = message;
    }

    public void startAlert(Dataset dataset){
        System.out.println(email);
        String printmessage = message.replace("%t", "%o");
        printmessage = printmessage.replace("%v", "%f");
        System.out.printf(Locale.US, printmessage+"\n", dataset.getZeit(), Math.round(100*dataset.getWert())/100.0);
    }
}