import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class Monitor{
    private List<Dataset> data;
    private List<Trigger> triggers;
    private List<Alert> alerts;

    Monitor() {
        this.data = new ArrayList<Dataset>();
        this.triggers = new ArrayList<Trigger>();
        this.alerts = new ArrayList<Alert>();
    }

    public void report(Dataset dataset) {
        data.add(dataset);
        if (checkTriggers())
            startAlerts();
    }

    public void addTrigger(Trigger t) {
        triggers.add(t);
    }

    public void addAlert(Alert a) {
        alerts.add(a);
    }

    public boolean checkTriggers() {
        for (Trigger t : triggers)
            if (t.isTriggered(data))
                return true;
        return false;
    }

    public void startAlerts() {
        for (Alert a : alerts)
            a.startAlert(data.get(data.size() - 1));
    }

}