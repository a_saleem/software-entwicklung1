public class Dataset {
    private long zeitstempel;
    private double wert;
    public Dataset(long zeitstempel, double wert){
        this.zeitstempel = zeitstempel;
        this.wert = wert;
    }

    public double getWert(){
        return this.wert;
    }

    public long getZeit(){
        return this.zeitstempel;
    }
}