import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;


/*interface Monitor{
    void report(Dataset);
}

interface Trigger extends Monitor{
    void addTrigger(AboveTrigger);
    void addTrigger(DeltaTrigger);
    //boolean setAlert(Alert);
}

interface Alert extends Monitor{
    void addAlert(TextAlert);
    void addAlert(MailAlert);
    //boolean setTrigger(Trigger);
}*/

public class Monitor {
    private List<Dataset> data;
    //private List<Trigger> triggers;
    //private List<Alert> alerts;
    private Dataset d;
    //private Trigger t;
    //private Alert a;
    private AboveTrigger aboveTrigger;
    private DeltaTrigger deltaTrigger;
    private TextAlert textAlert;
    private MailAlert mailAlert;

    Monitor(){
        this.data = new ArrayList<Dataset>();
        /*this.triggers = new ArrayList<Trigger>();
        this.alerts = new ArrayList<Alert>();*/
        this.aboveTrigger = new AboveTrigger(0);
        this.deltaTrigger = new DeltaTrigger(0, 0);
        this.textAlert = new TextAlert("No Text defined.");
        this.mailAlert = new MailAlert("","No Text defined.");
    }

    public void report(Dataset d){
        data.add(d);
        checkAlerts();
    }

    public void addTrigger(AboveTrigger t){
        //triggers.add(t);
        this.aboveTrigger = t;
    }

    public void addTrigger(DeltaTrigger t){
        //triggers.add(t);
        this.deltaTrigger = t;
    }

    public void addAlert(TextAlert a){
        //alerts.add(a);
        this.textAlert = a;
    }

    public void addAlert(MailAlert a){
        //alerts.add(a);
        this.mailAlert = a;
    }

    public void checkAlerts(){

    }


}