interface ListOfInt {
	void add(int element);
	int get(int index);
	int size();
	IteratorOfInt iterator();
}