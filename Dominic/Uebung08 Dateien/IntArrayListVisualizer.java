import java.util.Arrays;

// Verkürzte Implementierung der IntArrayList zur Ausfuehrung im Java-Visualizer
public class IntArrayList {
    private int[] elementData;
    private int size;

    public IntArrayList(int initialCapacity) {
      this.elementData = new int[initialCapacity];
      this.size = 0;
    }
    
    /** Element am Ende der Liste einfuegen */
    public void add(int element) {
        ensureCapacity(size + 1);
        elementData[size] = element;
        size++;
    }

    /** stellt sicher, dass Array genug Platz hat */
    private void ensureCapacity(int minCapacity) {
        if (elementData.length < minCapacity) {
            // Groesse verdoppeln, mindestens auf minCapacity
            int newSize = Math.max(minCapacity, 2*elementData.length);
            elementData = Arrays.copyOf(elementData, newSize);
        }
    }


   public static void main(String[] args){
   IntArrayList il = new IntArrayList(4);
   il.add(0);
   il.add(1);
   il.add(2);
   il.add(3);
   il.add(4);
   il.add(5);
   il.add(6);
   }
}