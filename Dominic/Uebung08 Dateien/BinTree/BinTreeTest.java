import static org.junit.Assert.*;

import org.junit.Test;

public class BinTreeTest {

    /**
     * Hilfsfunktion zum Erstellen eines Baums fuer Tests
     */
    private BinTree tree(int... elems) {
        BinTree tree = new BinTree();
        for (int e : elems) {
            tree.add(e);
        }
        return tree;
    }

    @Test
    public void test() {
        BinTree t = new BinTree();
        t.add(4);
        t.add(9);
        t.add(2);
        assertEquals(true, t.contains(4));
        assertEquals(false, t.contains(3));
        assertEquals(15, t.sum());
    }

    @Test
    public void testContains1() {
        BinTree tree = tree(5, 4, 6);
        assertTrue(tree.contains(4));
        assertTrue(tree.contains(5));
        assertTrue(tree.contains(6));
        assertFalse(tree.contains(3));
        assertFalse(tree.contains(7));
    }

    @Test
    public void testMax() {
        BinTree tree = tree(5, 2, 10, 15, 4, 1, 8);
        assertEquals(15, tree.max());
    }

    @Test
    public void testSize0() {
        BinTree tree = tree();
        assertEquals(0, tree.size());
    }

    @Test
    public void testSize7() {
        BinTree tree = tree(5, 2, 10, 15, 4, 1, 8);
        assertEquals(7, tree.size());
    }

    @Test
    public void testToArrayEmpty() {
        BinTree tree = tree();
        assertArrayEquals(new int[]{}, tree.toArray());
    }

    @Test
    public void testToArray1() {
        BinTree tree = tree(1, 2, 3);
        assertArrayEquals(new int[]{1, 2, 3}, tree.toArray());
    }

    @Test
    public void testToArray2() {
        BinTree tree = tree(2, 3, 1);
        assertArrayEquals(new int[]{1, 2, 3}, tree.toArray());
    }

    @Test
    public void testToArray3() {
        BinTree tree = tree(5, 8, 3, 9, 4, 6, 7, 1, 2);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, tree.toArray());
    }

}
