import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class SortedBinTreeTest {
    
    @Test
    public void randomTest_add() {
        List<Integer> theRealThing = new ArrayList<>();
        SortedBinTree set = new SortedBinTree();
        
        Random r = new Random(7);
        
        for (int i=0; i<1000; i++) {
            int x = r.nextInt(1000);
//            System.out.println("add " + x);
            theRealThing.add(x);
            set.add(x);
            Collections.sort(theRealThing);
            assertEquals(theRealThing.toString(), set.toString());
        }
    }
    
    @Test
    public void randomTest_addRemove() {
        List<Integer> theRealThing = new ArrayList<>();
        SortedBinTree set = new SortedBinTree();
        
        Random r = new Random(7);
        
        for (int i=0; i<10000; i++) {
            int x = r.nextInt(100);
            if (r.nextBoolean()) {
                System.out.println("add " + x);
                theRealThing.add(x);
                set.add(x);
            } else {
                System.out.println("remove " + x);
                theRealThing.remove((Integer) x);
                set.remove(x);
            }
            Collections.sort(theRealThing);
            assertEquals(theRealThing.toString(), set.toString());
        }
    }
    
    @Test
    public void randomTest_addRemove2() {
        List<Integer> theRealThing = new ArrayList<>();
        SortedBinTree set = new SortedBinTree();
        
        Random r = new Random(7);
        
        for (int i=0; i<10000; i++) {
            int x = r.nextInt(10);
            if (r.nextBoolean()) {
                System.out.println("add " + x);
                theRealThing.add(x);
                set.add(x);
            } else {
                System.out.println("remove " + x);
                theRealThing.remove((Integer) x);
                set.remove(x);
            }
            Collections.sort(theRealThing);
            assertEquals(theRealThing.toString(), set.toString());
        }
    }

}
