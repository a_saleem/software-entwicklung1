
class TreeNode {
    private int mark;
    private TreeNode left, right;

    public TreeNode(int element) {
        this.setMark(element);
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }

}

//Repraesentiert einen sortierten markierten Binaerbaum
public class SortedBinTree {
    private TreeNode root;

    public SortedBinTree() {
        root = null;
    }

    // prueft, ob ein Element im Baum enthalten ist
    public boolean contains(int element) {
        return contains(root, element);
    }

    private boolean contains(TreeNode node, int element) {
        if (node == null) {
            return false;
        }
        if (element < node.getMark()) {
            // kleinere Elemente links suchen
            return contains(node.getLeft(), element);
        } else if (element > node.getMark()) {
            // groessere Elemente rechts suchen
            return contains(node.getRight(), element);
        } else {
            // gefunden!
            return true;
        }
    }
    
    public void add(int element) {
        if (root == null) {
           root = new TreeNode(element);
        } else {
           add(root, element);
        }
     }

     private void add(TreeNode node, int element){
        if (element <= node.getMark()) {
           if (node.getLeft() == null) {
               node.setLeft(new TreeNode(element));
           } else {
               add(node.getLeft(), element);
           }
        } else {
           // Fall: node.getMark() < element
          if (node.getRight() == null) {
               node.setRight(new TreeNode(element));
           } else {
               add(node.getRight(), element);
           }
        }
     }

    /** fuegt ein Element in den Baum ein */
    public void addIterativ(int element) {
        if (root == null) {
            root = new TreeNode(element);
            return;
        }
        TreeNode node = root;
        while (true) {
            if (element <= node.getMark()) {
                // kleinere/gleiche Elemente links einfuegen
                if (node.getLeft() == null) {
                    node.setLeft(new TreeNode(element));
                    break;
                } else {
                    node = node.getLeft();
                }
            } else { // element > node.getMark()
                // groessere Elemente rechts einfuegen
                if (node.getRight() == null) {
                    node.setRight(new TreeNode(element));
                    break;
                } else {
                    node = node.getRight();
                }
            }
        }
    }

    /** entfernt ein Element aus dem Baum */
    public void remove(int element) {
        root = remove(root, element);
    }

    /** entfernt ein Element aus einem Teilbaum */
    private TreeNode remove(TreeNode node, int element) {
        if (node == null) {
            return null;
        }
        if (element < node.getMark()) {
            TreeNode newLeft = remove(node.getLeft(), element);
            node.setLeft(newLeft);
            return node;
        } else if (element > node.getMark()) {
            TreeNode newRight = remove(node.getRight(), element);
            node.setRight(newRight);
            return node;
        } else {
            // zu loeschendes Element gefunden
            if (node.getLeft() == null) {
                // wenn der linke Teilbaum leer ist, dann nur den rechten nehmen
                return node.getRight();
            } else if (node.getRight() == null) {
                // analog zu node.left == null
                return node.getLeft();
            } else {
                // wenn der Knoten zwei Kinder hat, den Knoten mit dem groessten
                // Element aus dem linken Teilbaum suchen:
                TreeNode maxNodeLeft = maxNode(node.getLeft());
                // Die Markierung von diesem Knoten nehmen wir fuer den aktuellen:
                node.setMark(maxNodeLeft.getMark());
                // Und dann loeschen wir das Element aus dem linken Teilbaum:
                node.setLeft(remove(node.getLeft(), node.getMark()));
                return node;
            }
        }
    }

    /** liefert den Knoten mit der groessten Markierung im Teilbaum */
    private TreeNode maxNode(TreeNode node) {
        if (node.getRight() == null) {
            return node;
        }
        return maxNode(node.getRight());
    }

    /** String Repraesentation des Baums in sortierter Reihenfolge  */
    public String toString() {
        return "[" + inorderToString(root) + "]";
    }

    private String inorderToString(TreeNode node) {
        if (node == null) {
            return "";
        }
        String smaller = inorderToString(node.getLeft());
        String bigger = inorderToString(node.getRight());

        String result = smaller;
        if (!smaller.isEmpty()) {
            result += ", ";
        }
        result += node.getMark();
        if (!bigger.isEmpty()) {
            result += ", ";
        }
        result += bigger;
        return result;
    }

    /** String Repraesentation des Baums in sortierter Reihenfolge  */
    public String toString2() {
        StringBuilder sb = new StringBuilder();
        inorderToString2(root, sb);
        return "[" + sb + "]";
    }

    private void inorderToString2(TreeNode node, StringBuilder sb) {
        if (node == null) {
            return;
        }
        inorderToString2(node.getLeft(), sb);
        if (sb.length() > 0) {
            sb.append(", ");
        }
        sb.append(node.getMark());
        inorderToString2(node.getRight(), sb);
    }

}
