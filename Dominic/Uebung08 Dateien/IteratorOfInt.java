interface IteratorOfInt {
	boolean hasNext();
	int next();
}