import static org.junit.Assert.*;

import org.junit.Test;

public class IntListTest {

    /*
     * Diese Prozedur erleichtert das Erstellen von Listen fuer Tests.
     * 
     * Hinweis:
     * Die Schreibweise "int ... elements" ist hier (fast) gleichbedeutend
     * mit der Schreibweise "int[] elements", das heisst elements ist ein
     * Array von int-Werten.
     * Allerdings kann man mit dieser Schreibweise die Prozedur auch direkt mit
     * beliebig vielen int-Werten aufrufen, zum Beispiel:
     *
     *  intlist(1,2,3)
     *
     * Die Werte werden dann automatisch in ein Array gepackt.
     */
    private IntList intlist(int... elements) {
        return new IntList(elements);
    }

    @Test
    public void testGet() {
        IntList list = intlist(4, 5, 6);
        assertEquals(4, list.get(0));
        assertEquals(5, list.get(1));
        assertEquals(6, list.get(2));
    }

    @Test
    public void testSize_empty() {
        assertEquals(0, intlist().size());
    }

    @Test
    public void testSize_nonempty() {
        assertEquals(1, intlist(0).size());
    }

    @Test
    public void testSize_nonempty2() {
        assertEquals(5, intlist(1, 2, 3, 4, 5).size());
    }

    @Test
    public void testSizeRec_empty() {
        assertEquals(0, intlist().sizeRec());
    }

    @Test
    public void testSizeRec_nonempty() {
        assertEquals(1, intlist(0).sizeRec());
    }

    @Test
    public void testSizeRec_nonempty2() {
        assertEquals(5, intlist(1, 2, 3, 4, 5).sizeRec());
    }

    @Test
    public void testContains1() {
        assertEquals(false, intlist().contains(5));
    }

    @Test
    public void testContains2() {
        assertEquals(true, intlist(7).contains(7));
    }

    @Test
    public void testContains3() {
        assertEquals(false, intlist(4, 6, 8, 10, 12).contains(7));
    }

    @Test
    public void testContains4() {
        assertEquals(true, intlist(4, 6, 8, 10, 12).contains(12));
    }

    @Test
    public void testToString1() {
        assertEquals("", intlist().toString());
    }

    @Test
    public void testToString2() {
        assertEquals("42", intlist(42).toString());
    }

    @Test
    public void testToString3() {
        assertEquals("7, 8, 9", intlist(7, 8, 9).toString());
    }

    @Test
    public void testAdd_index0() {
        IntList list = intlist(11, 22, 33, 44, 55, 66);
        list.add(0, 99);
        assertEquals("99, 11, 22, 33, 44, 55, 66", list.toString());
    }

    @Test
    public void testAdd_index1() {
        IntList list = intlist(11, 22, 33, 44, 55, 66);
        list.add(1, 42);
        assertEquals("11, 42, 22, 33, 44, 55, 66", list.toString());
    }

    @Test
    public void testAdd_index_end() {
        IntList list = intlist(11, 22, 33, 44, 55, 66);
        list.add(6, 17);
        assertEquals("11, 22, 33, 44, 55, 66, 17", list.toString());
    }

    @Test
    public void testAddIndexSequence() {
        IntList a = intlist(1, 2, 3);
        assertEquals("1, 2, 3", a.toString());
        a.add(3, 4);
        assertEquals("1, 2, 3, 4", a.toString());
        a.add(5);
        assertEquals("1, 2, 3, 4, 5", a.toString());
    }

    @Test
    public void testIterator() {
        IntList l = intlist(100, 20, 3);
        int sum = 0;
        IntListIterator iter = l.iterator();
        while (iter.hasNext()) {
            int elem = iter.next();
            sum = sum + elem;
        }
        assertEquals(123, sum);
    }
}
