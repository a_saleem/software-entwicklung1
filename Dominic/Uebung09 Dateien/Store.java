import java.util.*;

public class Store {

    // a)
    static List<Article> search(List<Article> l, String s) {
        List<Article> search = new ArrayList();
        for (Article a : l) {
            if (a.getName().contains(s) || a.getDescription().contains(s)) {
                search.add(a);
            }
        }
        return search;
    }

    // b)
    static long price(Order o) {
        long price = 0;
        List<Article> list = o.getArticles();
        for (Article a : list)
            price = price + a.getPrice();
        return price;
    }

    // c)
    static Set<Article> neverOrdered(List<Article> articles, List<Order> orders) {
        Set<Article> neverOrdered = new HashSet();
        for (Article a : articles) {
            neverOrdered.add(a);
        }
        for (Order o : orders) {
            List<Article> ordered = o.getArticles();
            for (Article a : ordered)
                neverOrdered.remove(a);
        }
        /*if (!orders.contains(a))
            neverOrdered.add(a);*/
        return neverOrdered;
    }

    // d)
    static Map<User, List<Article>> articlesByUser(List<Order> orders) {
        Map<User, List<Article>> articlesByUser = new LinkedHashMap();
        Set<User> users = new HashSet();
        // User werden in einem Set gespeichert
        for (Order o : orders) {
            users.add(o.getUser());
        }
        // Pro User werden alle Bestellungen überprüft
        for (User u : users) {
            List<Article> l = new ArrayList();
            for (Order o : orders) {
                // Bei Übereinstimmung wird die Bestellung an die Liste angehängt
                if (o.getUser().equals(u)) {
                    l.addAll(l.size(), o.getArticles());
                }
            }
            // Zusammengefasste Liste wird in der Map gespeichert
            articlesByUser.put(u, l);
        }
        return articlesByUser;
    }

    // e)
    /*
       static List<Order> mergeOrders(List<Order> orders) {
        List<Order> mergedOrders = new ArrayList();
        Map<User, List<Article>> articlesByUser = articlesByUser(orders);
        for (Map.Entry e : articlesByUser.entrySet()) {
            List<Article> tempArticle = e.getValue(); // Warum bringt mir e.getValue() keine List<Article>?
            Order temp = new StoreOrder(tempArticle, e.getKey());
            mergedOrders.add(temp);
        }
        return mergedOrders;
    }
    */

    static List<Order> mergeOrders(List<Order> orders) {
        List<Order> mergedOrders = new ArrayList();
        Set<User> users = new HashSet();
        for (Order o : orders) {
            users.add(o.getUser());
        }
        for (User u : users) {
            List<Article> a = new ArrayList();
            for (Order o : orders)
                if (o.getUser().equals(u))
                    a.addAll(0, o.getArticles());
            Order o = new StoreOrder(a, u);
            mergedOrders.add(o);
        }
        return mergedOrders;
    }
}

class StoreOrder implements Order {
    private final List<Article> articles;
    private final User user;

    public StoreOrder(List<Article> articles, User user) {
        this.articles = articles;
        this.user = user;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public User getUser() {
        return user;
    }
}