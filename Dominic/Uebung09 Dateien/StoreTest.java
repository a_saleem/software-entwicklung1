import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

interface Article {
    String getName();

    String getDescription();

    long getPrice();
}

interface Order {
    List<Article> getArticles();

    User getUser();
}

interface User {
    String getName();
}

class UserImpl implements User {

    private final String name;

    public UserImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserImpl user = (UserImpl) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "User " + name;
    }
}

class BusinessUser implements User {
    private final String companyName;
    private final String employeeName;

    public BusinessUser(String companyName, String employeeName) {
        this.companyName = companyName;
        this.employeeName = employeeName;
    }

    @Override
    public String getName() {
        return employeeName + " " + companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusinessUser user = (BusinessUser) o;
        return Objects.equals(companyName, user.companyName) &&
                Objects.equals(employeeName, user.employeeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyName, employeeName);
    }

    @Override
    public String toString() {
        return "User " + getName();
    }
}


class OrderImpl implements Order {
    private final List<Article> articles;
    private final User user;

    public OrderImpl(List<Article> articles, User user) {
        this.articles = articles;
        this.user = user;
    }

    @Override
    public List<Article> getArticles() {
        return articles;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Order from " + user + " " + articles;
    }
}

class ArticleImpl implements Article {

    private final String name;
    private final String description;
    private final long price;

    public ArticleImpl(String name, String description, long price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public long getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Article " + name + " (" +description + ";" + price + " cents)";
    }
}


public class StoreTest {

    private ArticleImpl banane = new ArticleImpl("banane", "gelbes obst", 149);
    private ArticleImpl apfel = new ArticleImpl("apfel", "rotes obst", 249);
    private ArticleImpl granatapfel = new ArticleImpl("granatapfel", "keine waffe", 300);
    private ArticleImpl granate = new ArticleImpl("granate", "gefaehrliche waffe", 1000000);

    @Test
    public void testSearchTitle() {
        List<Article> articles = Arrays.asList(banane, apfel, granatapfel, granate);
        List<Article> searchResult = Store.search(articles, "apfel");
        assertEquals(new LinkedHashSet<>(Arrays.asList(apfel, granatapfel)), new LinkedHashSet<>(searchResult));
    }

    @Test
    public void testSearchDescription() {
        List<Article> articles = Arrays.asList(banane, apfel, granatapfel, granate);
        List<Article> searchResult = Store.search(articles, "obst");
        assertEquals(new LinkedHashSet<>(Arrays.asList(apfel, banane)), new LinkedHashSet<>(searchResult));
    }


    @Test
    public void testPrice() {
        Order order = new OrderImpl(Arrays.asList(apfel, apfel, banane), new UserImpl("Hans Wurst"));
        assertEquals(647, Store.price(order));
    }

    @Test
    public void testNeverOrdered() {
        List<Article> articles = Arrays.asList(banane, apfel, granatapfel, granate);
        List<Order> orders = Arrays.asList(
                new OrderImpl(Arrays.asList(apfel, apfel), new UserImpl("Alice")),
                new OrderImpl(Arrays.asList(apfel, granatapfel), new UserImpl("Bob"))

        );
        assertEquals(new LinkedHashSet<>(Arrays.asList(granate, banane)), Store.neverOrdered(articles, orders));
    }

    @Test
    public void testArticlesByUser() {
        List<Order> orders = Arrays.asList(
                new OrderImpl(Arrays.asList(apfel, apfel), new UserImpl("Alice")),
                new OrderImpl(Arrays.asList(apfel, granatapfel), new UserImpl("Bob")),
                new OrderImpl(Arrays.asList(banane, banane, apfel), new UserImpl("Alice"))
        );
        Map<User, List<Article>> byUser = Store.articlesByUser(orders);
        assertEquals(new Multiset<>(apfel, apfel, banane, banane, apfel), new Multiset<>(byUser.get(new UserImpl("Alice"))));
        assertEquals(new Multiset<>(apfel, granatapfel), new Multiset<>(byUser.get(new UserImpl("Bob"))));
        assertEquals(2, byUser.size());
    }

    @Test
    public void testMergeOrders() {
        List<Order> orders = Arrays.asList(
                new OrderImpl(Arrays.asList(apfel, apfel), new UserImpl("Alice")),
                new OrderImpl(Arrays.asList(apfel, granatapfel), new UserImpl("Bob")),
                new OrderImpl(Arrays.asList(banane, banane, apfel), new UserImpl("Alice"))
        );
        List<Order> optimized = Store.mergeOrders(orders);
        assertEquals(2, optimized.size());
        Order aliceOrder = optimized.get(0);
        Order bobOrder = optimized.get(1);
        if (!aliceOrder.getUser().getName().equals("Alice")) {
            Order temp = aliceOrder;
            aliceOrder = bobOrder;
            bobOrder = temp;
        }
        assertEquals(new UserImpl("Alice"), aliceOrder.getUser());
        assertEquals(new UserImpl("Bob"), bobOrder.getUser());
        assertEquals(new Multiset<>(apfel, apfel, banane, banane, apfel), new Multiset<>(aliceOrder.getArticles()));
        assertEquals(new Multiset<>(apfel, granatapfel), new Multiset<>(bobOrder.getArticles()));
    }

    @Test
    public void testMergeOrders2() {
        List<Order> orders = Arrays.asList(
                new OrderImpl(Arrays.asList(apfel, apfel), new UserImpl("Alice Mueller")),
                new OrderImpl(Arrays.asList(apfel, granatapfel), new UserImpl("Bob")),
                new OrderImpl(Arrays.asList(banane, banane, apfel), new BusinessUser("Mueller", "Alice"))
        );
        List<Order> optimized = Store.mergeOrders(orders);
        assertEquals(3, optimized.size());
    }

    // Multiset wird verwendet, um zu pruefen, ob zwei Listen den gleichen Inhalt in eventuell unterschiedlicher Reihenfolge haben
    static class Multiset<T> {
        private Map<T, Integer> counts = new LinkedHashMap<>();

        Multiset(T... ts) {
            this(Arrays.asList(ts));
        }

        Multiset(Collection<T> ts) {
            for (T t : ts) {
                counts.put(t, counts.getOrDefault(t, 0) + 1);
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Multiset<?> mutliset = (Multiset<?>) o;
            return Objects.equals(counts, mutliset.counts);
        }

        @Override
        public int hashCode() {
            return Objects.hash(counts);
        }

        @Override
        public String toString() {
            return counts.toString();
        }
    }



}
