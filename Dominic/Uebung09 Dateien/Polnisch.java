public class Polnisch {
    public static void main(String args[]) {
        StackOfInt stack = new ArrayStackOfInt(100);
        while (!StdIn.isEmpty()) {
            String s = StdIn.readString();
            if (Character.isDigit(s.charAt(0))) { //ist die erste Stelle des Strings eine Ziffer?
                stack.push(Integer.parseInt(s)); //Packe den Integer-Wert auf den Stapel
            } else {
                switch (s) {
                case "+":
                    stack.push(stack.pop() + stack.pop()); //Nimmt die beiden obersten Zahlen vom Stapel, addiert sie, und legt das Ergebnis wieder auf den Stapel
                    break;
                case "-":
                    stack.push(-stack.pop() + stack.pop()); //Bei Subtraktion ist die Reihenfolge wichtig
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    int x = stack.pop();
                    stack.push(stack.pop() / x); //Hilfsvariable, um eine Integer-Division in der richtigen Reihenfolge durchführen zu können
                    break;

                }
            }
        }
        System.out.println(stack.peek());
    }
}