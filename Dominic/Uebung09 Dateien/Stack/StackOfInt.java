interface StackOfInt {

  // Testet, ob der Stapel leer ist.
  boolean empty();

  // Legt Element oben auf den Stapel.
  void push(int item);

  // Gibt das oberste Element vom Stapel zurueck.
  int peek();

  // Gibt das oberste Element vom Stapel zurueck
  // und entfernt es vom Stapel.
  int pop();
}
