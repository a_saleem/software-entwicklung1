import java.util.EmptyStackException;

public class ListStackOfInt implements StackOfInt {
  private Node first;

  public ListStackOfInt() { }

  public boolean empty() {
    return (first == null);
  }
  public void push(int item) {
    Node n = new Node(item, first);
    first = n;
  }
  public int peek() {
    if (first == null) {
      throw new EmptyStackException();
    }
    return first.getItem();
  }
  public int pop() {
    int result = peek();
    first = first.getNext();
    return result;
  }
}
