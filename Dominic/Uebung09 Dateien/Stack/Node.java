class Node {
  int item;
  Node next;
  Node(int item, Node next) { this.item = item; this.next = next; }
  int getItem()  { return item; }
  Node getNext() { return next; }
  void setNext(Node next) { this.next = next; }
}
