import java.util.EmptyStackException;

public class ArrayStackOfInt implements StackOfInt {
  private int[] elems;
  private int n;

  public ArrayStackOfInt(int max) {
    this.elems = new int[max];
  }
  public boolean empty() {
    return n == 0;
  }
  public void push(int item) {
      elems[n] = item;
      n++;
  }
  public int peek() {
    if (empty()) throw new EmptyStackException();
    return elems[n-1];
  }
  public  int pop() {
    int result = this.peek();
    n--;
    return result;
  }
}
