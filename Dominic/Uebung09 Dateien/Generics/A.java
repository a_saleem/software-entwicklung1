class A<S, T> {
    public S x;
    public T y;
}

class B<S> extends A<String, S> {
    S f() {
        return y; //x ist bei einem Objekt der Klasse B vom Typ String, y vom Typ S
    }

    String g() {
        return this.x;
    }

    String h() {
        return x; //x ist bei einem Objekt der Klasse B vom Typ String, y vom Typ S
    }
}

//Frage: Wird durch die extends Angabe in dieser Form auch definiert, dass String ein Subtyp von S ist und S ein Subtyp von T?