import java.util.*;

class Cell<T> {
        private T t;

        void set(T t) {
            this.t = t;
        }

        T get() {
            return t;
        }
    }

    class CellExample {
        void test() {
            List<Cell> cells = new ArrayList<Cell>();
            cells.add(new Cell());
            System.out.println(cells);
        }
    }