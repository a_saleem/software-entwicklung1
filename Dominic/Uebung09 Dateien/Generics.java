import java.util.*;

public class Generics {
    public static void main(String[] args) {
        // a)
        ListOfInts.test();
        // b)
        CellExample.test();
        // c)
        new Mensch().essen(new Korb<Banane>());
    }
}

// a)
class ListOfInts {
    public static void test() { // "static" ergänzt, da die Methode nicht auf einem Objekt angewandt wird
        List<Integer> list = new LinkedList<Integer>(); // Linked List benötigt Objekttypen ("Integer" als Wrapper-Klasse von "int")
        list.add(37);
        System.out.println(list);
    }
}

// b)
class Cell<T> {
    private T t;

    void set(T t) {
        this.t = t;
    }

    T get() {
        return t;
    }
}

class CellExample {
    static void test() {
        List<Cell> cells = new ArrayList<Cell>(); // <T> entfernt, da die Typvariable im Konstruktor deklariert wird
        cells.add(new Cell()); // <T> entfernt
        System.out.println(cells);
    }
}

// c)
class Korb<T>{
}

class Obst {
}

class Banane extends Obst {
}

class Mensch {
    void essen(Korb<? extends Obst> s) { // Die Suptyp Beziehung der Typvariablen wird nicht automatisch durch die Klassen-Beziehung übernommen
        System.out.println("hmm, lecker");
    }

    // nach oben in die main Methode gepackt 
    /*
    public static void main(String[] args) {
        new Mensch().essen(new Korb<Banane>());
    }
    */
}

// d)
class A<S, T> {
    public S x;
    public T y;
}

class B<S> extends A<String, S> {
    S f() {
        return y; //x ist bei einem Objekt der Klasse B vom Typ String, y vom Typ S
    }

    String g() {
        return this.x;
    }

    String h() {
        return x; //x ist bei einem Objekt der Klasse B vom Typ String, y vom Typ S
    }
}

//Frage: Wird durch die extends Angabe in dieser Form auch definiert, dass String ein Subtyp von S ist und S ein Subtyp von T?
