public class ReadStrings {

    public static void main(String[] args) {
        /*Test
        String[] Test = readStrings();
        for (int i=0; i<Test.length; i++)
        System.out.println(Test[i]);   */ 
    }

    public static String[] readStrings() {
        int n = 0; //Zählt die Eingaben
        boolean aktuelltemp1 = true; //artemp1 ist das aktuelle Array
        String [] artemp1 = new String[5];
        String [] artemp2 = new String[5];
        String Eingabe;
        while (!StdIn.isEmpty()) {
            Eingabe = StdIn.readString();     
            if (aktuelltemp1) {
                if (n<artemp1.length)
                    artemp1[n]=Eingabe;
                else {                    
                    artemp2 = new String[artemp1.length*2];
                    for (int i=0; i<artemp1.length; i++) //kopiert das zu kleine Array in das neue Array
                        artemp2[i] = artemp1 [i];
                    artemp2[artemp1.length]=Eingabe; //Eingabe in das nächste freie Element
                    aktuelltemp1 = false;
                }
            }    
            else {
                    if (n<artemp2.length)
                        artemp2[n]=Eingabe;
                    else {
                        artemp1 = new String[artemp2.length*2];
                        for (int i=0; i<artemp2.length; i++) //kopiert das zu kleine Array in das neue Array
                            artemp1[i] = artemp2 [i];
                        artemp1[artemp2.length]=Eingabe; //Eingabe in das nächste freie Element
                        aktuelltemp1 = true;
                    }
            }
            n++;               
        }   
        String arfinal [] = new String[n]; //Finales Array mit der Länge n
        for (int i=0; i<n; i++)
            if (aktuelltemp1) arfinal[i]=artemp1[i];
            else arfinal[i]=artemp2[i];
        return arfinal;
    }

}